﻿/*using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BnkStmntSearch.Database;
using EntityFileInfo = BnkStmntSearch.Data.Entities.Light.FileInfo;

namespace BnkStmntSearch.Data.Entities.Light.Manager
{
    public class FileInfoManager : IFileInfoManager
    {
        public const string DateTimeFormat = "MM-dd-yyyy hh:mm:ss tt";

        public IList<EntityFileInfo> GetList()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.LightFileInfo
                    .Include(i => i.BankStatements)
                    .ToList()
                    .Select(i => new EntityFileInfo(i))
                    .ToList();
            }
        }

        public IList<EntityFileInfo> GetListWithBankStatements()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.LightFileInfo
                    .Include(i => i.BankStatements)
                    .ToList()
                    .Select(i => new EntityFileInfo(i, true))
                    .ToList();
            }
        }
    }
}*/