﻿/*using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using BnkStmntSearch.Database;
using DatabaseBankStatement = BnkStmntSearch.Database.Model.Light.BankStatement;

namespace BnkStmntSearch.Data.Entities.Light
{
    public class BankStatement
    {
        public BankStatement(DatabaseBankStatement bankStatement)
        {
            Id = bankStatement.Id;
            LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime;
            HPage = bankStatement.HPage;
            GBasicHeader = bankStatement.GBasicHeader;
            GApplication = bankStatement.GApplication;
            Trn = bankStatement.Trn;
            AccountIdentification = bankStatement.AccountIdentification;
            StatementNo = bankStatement.StatementNo;
            StatementSeqNo = bankStatement.StatementSeqNo;
            OpenBalanceIndicator = bankStatement.OpenBalanceIndicator;
            OpenBalanceDate = bankStatement.OpenBalanceDate;
            OpenBalanceCurrency = bankStatement.OpenBalanceCurrency;
            OpenBalanceAmount = bankStatement.OpenBalanceAmount;
            Guid = bankStatement.Guid;
            StatementContent = bankStatement.StatementContent;

            if (bankStatement.FileInfo != null)
                FileInfo = new FileInfo(bankStatement.FileInfo);

            if (bankStatement.StatementLines != null && bankStatement.StatementLines.Count > 0)
                StatementLines = bankStatement.StatementLines
                    .Select(i => new StatementLine(i,false)
                    {
                        BankStatementId = Id,
                        BankStatement = this
                    })
                    .ToList();
        }
        public BankStatement(DatabaseBankStatement bankStatement, FileInfo fileInfo)
        {
            Id = bankStatement.Id;
            LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime;
            HPage = bankStatement.HPage;
            GBasicHeader = bankStatement.GBasicHeader;
            GApplication = bankStatement.GApplication;
            Trn = bankStatement.Trn;
            AccountIdentification = bankStatement.AccountIdentification;
            StatementNo = bankStatement.StatementNo;
            StatementSeqNo = bankStatement.StatementSeqNo;
            OpenBalanceIndicator = bankStatement.OpenBalanceIndicator;
            OpenBalanceDate = bankStatement.OpenBalanceDate;
            OpenBalanceCurrency = bankStatement.OpenBalanceCurrency;
            OpenBalanceAmount = bankStatement.OpenBalanceAmount;
            Guid = bankStatement.Guid;
            StatementContent = bankStatement.StatementContent;

            if (fileInfo != null)
                FileInfo = fileInfo;
            else if (bankStatement.FileInfo != null)
                FileInfo = new FileInfo(bankStatement.FileInfo);

            if (bankStatement.StatementLines != null && bankStatement.StatementLines.Count > 0)
                StatementLines = bankStatement.StatementLines
                    .Select(i => new StatementLine(i, false)
                    {
                        BankStatementId = Id,
                        BankStatement = this
                    })
                    .ToList();

            if(StatementLines == null || StatementLines.Count == 0)
                QueryStatementLines();
        }

        private void QueryStatementLines()
        {
            using (var context = new DatabaseContext())
            {
                var dbStatementLines = context.LightStatementLine
                    .Where(i => i.BankStatementId == Id)
                    .ToList();
                if(dbStatementLines.Count > 0)
                    StatementLines = dbStatementLines
                        .Select(i => new StatementLine(i, false)
                        {
                            BankStatementId = Id,
                            BankStatement = this
                        })
                        .ToList();
                else
                    StatementLines = new List<StatementLine>();
            }
        }

        public BankStatement()
        {
            StatementLines = new List<StatementLine>();
        }

        public long Id { get; set; }
        public string LogicalTerminalDateTime { get; set; }
        public string HPage { get; set; }
        public string GBasicHeader { get; set; }
        public string GApplication { get; set; }
        public string Trn { get; set; }
        public string AccountIdentification { get; set; }
        public string StatementNo { get; set; }
        public string StatementSeqNo { get; set; }
        public string OpenBalanceIndicator { get; set; }
        public string OpenBalanceDate { get; set; }
        public string OpenBalanceCurrency { get; set; }
        public string OpenBalanceAmount { get; set; }
        public string StatementContent { get; set; }
        public string Guid { get; set; }
        public FileInfo FileInfo { get; set; }
        public IList<StatementLine> StatementLines { get; set; }
    }
}*/