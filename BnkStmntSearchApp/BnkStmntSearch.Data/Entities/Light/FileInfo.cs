﻿/*using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DatabaseFileInfo = BnkStmntSearch.Database.Model.Light.FileInfo;

namespace BnkStmntSearch.Data.Entities.Light
{
    public class FileInfo
    {
        public const string DateTimeFormat = "MM-dd-yyyy hh:mm:ss tt";

        public FileInfo(DatabaseFileInfo fileInfo)
        {
            Id = fileInfo.Id;
            FileName = fileInfo.FileName;
            UploadDate = fileInfo.UploadDate;
            UploadedBy = fileInfo.UploadedBy;
        }

        public FileInfo(DatabaseFileInfo fileInfo, bool isWithBankStatement)
        {
            Id = fileInfo.Id;
            FileName = fileInfo.FileName;
            UploadDate = fileInfo.UploadDate;
            UploadedBy = fileInfo.UploadedBy;
            if (fileInfo.BankStatements != null)
                BankStatements = fileInfo.BankStatements
                    .Select(i => new BankStatement(i, this))
                    .ToList();

        }

        public FileInfo() { }

        public long Id { get; set; }
        public string FileName { get; set; }
        public string UploadDate { get; set; }
        public string UploadedBy { get; set; }

        public IList<BankStatement> BankStatements { get; set; }

        public DateTime GetUploadDate()
        {
            return DateTime.ParseExact(UploadDate, DateTimeFormat, CultureInfo.InvariantCulture);
        }
    }
}*/