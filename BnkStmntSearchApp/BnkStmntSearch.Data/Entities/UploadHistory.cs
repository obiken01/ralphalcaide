﻿using System;
using System.Globalization;
using DbUploadHistory = BnkStmntSearch.Database.Model.UploadHistory;

namespace BnkStmntSearch.Data.Entities
{
    public class UploadHistory
    {
        public const string DateTimeFormat = "MM-dd-yyyy hh:mm:ss tt";

        public UploadHistory(DbUploadHistory uploadHistory)
        {
            Id = uploadHistory.Id;
            FileName = uploadHistory.FileName;
            UploadDate = uploadHistory.UploadDate;
            UploadedBy = uploadHistory.UploadedBy;
        }

        public UploadHistory() { }

        public long Id { get; set; }
        public string FileName { get; set; }
        public string UploadDate { get; set; }
        public string UploadedBy { get; set; }

        public DateTime GetUploadDate()
        {
            return DateTime.ParseExact(UploadDate, DateTimeFormat, CultureInfo.InvariantCulture);
        }
    }
}