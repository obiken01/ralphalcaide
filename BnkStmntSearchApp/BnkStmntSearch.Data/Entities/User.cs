﻿using BnkStmntSearch.Data.Enum;
using DatabaseUser = BnkStmntSearch.Database.Model.User;

namespace BnkStmntSearch.Data.Entities
{
    public class User
    {
        public User(DatabaseUser dbUser)
        {
            Id = dbUser.Id;
            UserId = dbUser.UserId;
            Password = dbUser.Password;
            Role = (UserRole) dbUser.Role;
        }

        public long Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
    }
}