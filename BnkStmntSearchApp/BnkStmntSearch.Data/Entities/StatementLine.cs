﻿using DatabaseStatementLine = BnkStmntSearch.Database.Model.StatementLine;

namespace BnkStmntSearch.Data.Entities
{
    public class StatementLine
    {
        public StatementLine(DatabaseStatementLine statementLine)
        {
            Id = statementLine.Id;
            Sequence = statementLine.Sequence;
            Date = statementLine.Date;
            Currency = statementLine.Currency;
            Amount = statementLine.Amount;
            ApplicationNumber = statementLine.ApplicationNumber;
            RefServer = statementLine.RefServer;
            ChequeNo = statementLine.ChequeNo;
            Guid = statementLine.Guid;

            if (statementLine.BankStatementId > -1)
                BankStatementId = statementLine.BankStatementId;
            if (statementLine.BankStatement != null)
                BankStatement = new BankStatement(statementLine.BankStatement);
        }

        public StatementLine(DatabaseStatementLine statementLine, bool mapBankStatement)
        {
            Id = statementLine.Id;
            Sequence = statementLine.Sequence;
            Date = statementLine.Date;
            Currency = statementLine.Currency;
            Amount = statementLine.Amount;
            ApplicationNumber = statementLine.ApplicationNumber;
            RefServer = statementLine.RefServer;
            ChequeNo = statementLine.ChequeNo;
            Guid = statementLine.Guid;

            if (!mapBankStatement) return;
            if (statementLine.BankStatementId > -1)
                BankStatementId = statementLine.BankStatementId;
            if (statementLine.BankStatement != null)
                BankStatement = new BankStatement(statementLine.BankStatement);
    }

        public StatementLine() { }

        public long Id { get; set; }
        public string Sequence { get; set; }
        public string Date { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string ApplicationNumber { get; set; }
        public string RefServer { get; set; }
        public string ChequeNo { get; set; }
        public string Guid { get; set; }
        public long BankStatementId { get; set; }
        public BankStatement BankStatement { get; set; }
    }
}