﻿using System.Collections.Generic;
using BnkStmntSearch.Data.Entities;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public interface IUploadHistoryManager
    {
        IList<UploadHistory> GetList();
        bool AddUploadHistory(string fileName, string uploadDate, string uploadBy, string dateDeleted);
        bool AddUploadHistory(UploadHistory uploadHistory, FileInfo fileInfo);
    }
}
