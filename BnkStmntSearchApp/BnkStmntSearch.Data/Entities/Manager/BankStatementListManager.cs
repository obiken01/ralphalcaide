﻿using System;
using System.Collections.Generic;
using System.Reflection;
using log4net;
using EntityBankStatement = BnkStmntSearch.Data.Entities.BankStatement;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public sealed class BankStatementListManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IFileInfoManager _manager;

        private List<EntityBankStatement> _list;

        private BankStatementListManager()
        {
            _manager = new FileInfoManager();
        }

        public static BankStatementListManager Instance { get; } = new BankStatementListManager();

        public IList<EntityBankStatement> GetList()
        {
            if (_list == null)
                UpdateList();
            return _list;
        }

        public bool UpdateList()
        {
            try
            {
                var fileInfos = _manager.GetListWithBankStatements();
                _list = new List<BankStatement>();
                foreach (var fileInfo in fileInfos)
                {
                    _list.AddRange(fileInfo.BankStatements);
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}