﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BnkStmntSearch.Database;
using log4net;
using log4net.Repository.Hierarchy;
using DbFileInfo = BnkStmntSearch.Database.Model.FileInfo;
using DbBankStatement = BnkStmntSearch.Database.Model.BankStatement;
using DbStatementLine = BnkStmntSearch.Database.Model.StatementLine;
using EntityFileInfo = BnkStmntSearch.Data.Entities.FileInfo;
using EntityBankStatement = BnkStmntSearch.Data.Entities.BankStatement;
using System.Data.Entity;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public class FileInfoManager : IFileInfoManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public const string DateTimeFormat = "MM-dd-yyyy hh:mm:ss tt";

        public IList<EntityFileInfo> GetList()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.FileInfo
                    .Include(i => i.BankStatements)
                    .ToList()
                    .Select(i => new EntityFileInfo(i))
                    .ToList();
            }
        }

        public IList<FileInfo> GetLightList()
        {
            throw new NotImplementedException();
        }

        /*public IList<EntityFileInfo> GetLightList()
        {
            using (var dbContext = new DatabaseContext())
            {
                var result = (from fI in dbContext.FileInfo
                        join b in dbContext.BankStatement on fI.Id equals b.FileInfoId
                        join s in dbContext.StatementLine on b.Id equals s.BankStatementId
                        select new EntityFileInfo
                        {
                            Id = fI.Id,
                            FileName = fI.FileName,
                            UploadDate = fI.UploadDate,
                            UploadedBy = fI.UploadedBy
                        })
                    .ToList();
                foreach (var fileInfo in result)
                {
                    /*var bankResults = (from b in dbContext.BankStatement
                            where b.FileInfoId == fileInfo.Id
                            select new EntityBankStatement(b, fileInfo))
                        .ToList();#1#
                    
                    var bankResults = dbContext.BankStatement

                    fileInfo.BankStatements = bankResults;
                }
                return result;
            }
        }*/

        public IList<EntityFileInfo> GetListWithBankStatements()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.FileInfo
                    .Include(i => i.BankStatements)
                    .Include(i => i.BankStatements.Select(s => s.StatementLines))
                    .ToList()
                    .Select(i => new EntityFileInfo(i,false))
                    .ToList();
            }
        }

        public bool DeleteFileInfo(EntityFileInfo fileInfo)
        {
            if(fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var dbFileInfo = dbContext.FileInfo.SingleOrDefault(i => i.Id == fileInfo.Id);
                    if (dbFileInfo == null)
                        throw new Exception($"File info {fileInfo.FileName}|{fileInfo.Id} was not found from the database.");
                    dbContext.FileInfo.Remove(dbFileInfo);
                    dbContext.SaveChanges();
                    _logger.Info("File Info has been deleted from the Database");
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool AddFileInfo(EntityFileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));
            return AddFileInfo(fileInfo.FileName, fileInfo.GetUploadDate(), fileInfo.UploadedBy, fileInfo.File);
        }

        public bool AddFileInfo(string fileName, DateTime uploadDate, string uploadedBy, byte[] file)
        {
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var newFileInfo = new DbFileInfo
                    {
                        FileName = fileName,
                        UploadDate = uploadDate.ToString(DateTimeFormat),
                        UploadedBy = uploadedBy,
                        File = file
                    };

                    dbContext.FileInfo.Add(newFileInfo);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool AddFullFileInfo(EntityFileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));

            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var newFileInfo = new DbFileInfo
                    {
                        FileName = fileInfo.FileName,
                        UploadDate = fileInfo.GetUploadDate().ToString(DateTimeFormat),
                        UploadedBy = fileInfo.UploadedBy,
                        File = fileInfo.File,
                        BankStatements = PopulateBankStatements(fileInfo)
                    };

                    dbContext.FileInfo.Add(newFileInfo);
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        private IList<DbBankStatement> PopulateBankStatements(EntityFileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));

            return fileInfo.BankStatements.Select(bankStatement => new DbBankStatement
                {
                    LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime,
                    HPage = bankStatement.HPage,
                    GBasicHeader = bankStatement.GBasicHeader,
                    GApplication = bankStatement.GApplication,
                    Trn = bankStatement.Trn,
                    AccountIdentification = bankStatement.AccountIdentification,
                    StatementNo = bankStatement.StatementNo,
                    StatementSeqNo = bankStatement.StatementSeqNo,
                    OpenBalanceIndicator = bankStatement.OpenBalanceIndicator,
                    OpenBalanceDate = bankStatement.OpenBalanceDate,
                    OpenBalanceCurrency = bankStatement.OpenBalanceCurrency,
                    OpenBalanceAmount = bankStatement.OpenBalanceAmount,
                    StatementContent = bankStatement.StatementContent,
                    Guid = bankStatement.Guid,
                    StatementLines = PopulateStatementLines(bankStatement)
            }).ToList();
        }

        private IList<DbStatementLine> PopulateStatementLines(EntityBankStatement bankStatement)
        {
            if (bankStatement == null)
                throw new ArgumentNullException(nameof(bankStatement));

            return bankStatement.StatementLines.Select(statementLine => new DbStatementLine
            {
                Sequence = statementLine.Sequence,
                Date = statementLine.Date,
                Currency = statementLine.Currency,
                Amount = statementLine.Amount,
                ApplicationNumber = statementLine.ApplicationNumber,
                RefServer = statementLine.RefServer,
                ChequeNo = statementLine.ChequeNo,
                Guid = statementLine.Guid
            }).ToList();
        }
    }
}
