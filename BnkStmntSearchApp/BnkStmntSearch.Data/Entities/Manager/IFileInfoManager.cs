﻿using System;
using System.Collections.Generic;
using FileInfoEntity = BnkStmntSearch.Data.Entities.FileInfo;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public interface IFileInfoManager
    {
        IList<FileInfoEntity> GetList();
        IList<FileInfoEntity> GetLightList();
        IList<FileInfoEntity> GetListWithBankStatements();
        bool DeleteFileInfo(FileInfoEntity fileInfo);
        bool AddFileInfo(FileInfoEntity fileInfo);
        bool AddFileInfo(string fileName, DateTime uploadDate, string uploadedBy, byte[] file);
        bool AddFullFileInfo(FileInfoEntity fileInfo);
    }
}