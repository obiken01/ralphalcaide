﻿using System.Collections.Generic;
using BnkStmntSearch.Data.Entities;
using BnkStmntSearch.Data.Enum;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public interface IUserManager
    {
        IList<User> GetList();
        User GetUserById(long id);
        User GetUser(string userId, UserRole role);
        bool AddUser(string userId, string password, UserRole role);
        bool UpdateUser(User user);
        bool DeleteUser(User user);
        User LoginUser(string userId, string password, UserRole role);
    }
}