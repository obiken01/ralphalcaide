﻿using System.Collections.Generic;
using BnkStmntSearch.Data.Entities;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public interface IBankStatementManager
    {
        IList<BankStatement> GetList();
        bool AddBankStatements(IList<BankStatement> bankStatementList, FileInfo fileInfo);
        bool DeleteBankStatements(IList<BankStatement> bankStatementList);
        bool AddBankStatement(BankStatement bankStatement, FileInfo fileInfo);
        bool AddBankStatementComplete(BankStatement bankStatement, FileInfo fileInfo);
    }
}
