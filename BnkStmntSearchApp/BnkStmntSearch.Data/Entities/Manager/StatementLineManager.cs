﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BnkStmntSearch.Database;
using log4net;
using DbStatementLine = BnkStmntSearch.Database.Model.StatementLine;
using DbBankStatement = BnkStmntSearch.Database.Model.BankStatement;
using EntityStatementLine = BnkStmntSearch.Data.Entities.StatementLine;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public class StatementLineManager : IStatementLineManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<EntityStatementLine> GetList()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.StatementLine.ToList().Select(i => new EntityStatementLine(i)).ToList();
            }
        }

        public bool AddStatementLines(IList<EntityStatementLine> statementLines, BankStatement bankStatement)
        {
            try
            {
                if(bankStatement == null)
                    throw new ArgumentNullException(nameof(bankStatement));
                using (var dbContext = new DatabaseContext())
                {
                    var dbBankStatement = dbContext.BankStatement.SingleOrDefault(i => i.Guid == bankStatement.Guid);
                    if (dbBankStatement == null)
                        throw new Exception($"File {bankStatement.Guid} was not found on the database.");
                    var dbStatementLineList = statementLines.Select(i => new DbStatementLine
                    {
                        Sequence = i.Sequence,
                        Date = i.Date,
                        Currency = i.Currency,
                        Amount = i.Amount,
                        ApplicationNumber = i.ApplicationNumber,
                        RefServer = i.RefServer,
                        ChequeNo = i.ChequeNo,
                        Guid = i.Guid,
                        BankStatement = dbBankStatement
                    })
                    .ToList();
                    dbContext.StatementLine.AddRange(dbStatementLineList);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool AddStatementLines(IList<EntityStatementLine> statementLines)
        {
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    DbBankStatement dbBankStatement = null;
                    var dbStatementLineList = new List<DbStatementLine>();
                    foreach (var statementLine in statementLines)
                    {
                        if (statementLine.BankStatement == null)
                            throw new NullReferenceException("statementLine.BankStatement");
                        
                        if (dbBankStatement == null || dbBankStatement.Guid != statementLine.BankStatement.Guid)
                        {
                            dbBankStatement = dbContext.BankStatement.SingleOrDefault(i => i.Guid == statementLine.BankStatement.Guid);
                            if (dbBankStatement == null)
                                throw new Exception($"File {statementLine.BankStatement.Guid} was not found on the database.");
                        }
                        dbStatementLineList.Add(new DbStatementLine
                        {
                            Sequence = statementLine.Sequence,
                            Date = statementLine.Date,
                            Currency = statementLine.Currency,
                            Amount = statementLine.Amount,
                            ApplicationNumber = statementLine.ApplicationNumber,
                            RefServer = statementLine.RefServer,
                            ChequeNo = statementLine.ChequeNo,
                            Guid = statementLine.Guid,
                            BankStatement = dbBankStatement
                        });
                    }
                    dbContext.StatementLine.AddRange(dbStatementLineList);
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool DeleteStatementLines(BankStatement bankStatement)
        {
            try
            {
                if (bankStatement == null)
                    throw new ArgumentNullException(nameof(bankStatement));
                using (var dbContext = new DatabaseContext())
                {
                    var dbStatementLines = dbContext.StatementLine
                        .Where(i => i.BankStatementId == bankStatement.Id)
                        .ToList();
                    if (dbStatementLines.Count == 0)
                        throw new Exception("No statement line records found.");
                    dbContext.StatementLine.RemoveRange(dbStatementLines);
                    dbContext.SaveChanges();
                    _logger.Info("Bank Statement has been deleted from the Database");
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool DeleteStatementLines(IList<BankStatement> bankStatements)
        {
            try
            {
                if (bankStatements == null)
                    throw new ArgumentNullException(nameof(bankStatements));
                var bankStatementIdList = bankStatements.Select(i => i.Id).ToList();
                using (var dbContext = new DatabaseContext())
                {
                    var dbStatementLines = dbContext.StatementLine
                        .Where(i => bankStatementIdList.Contains(i.BankStatementId))
                        .ToList();
                    if (dbStatementLines.Count == 0)
                        throw new Exception("No statement line records found.");
                    dbContext.StatementLine.RemoveRange(dbStatementLines);
                    dbContext.SaveChanges();
                    _logger.Info("Bank Statement has been deleted from the Database");
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}
