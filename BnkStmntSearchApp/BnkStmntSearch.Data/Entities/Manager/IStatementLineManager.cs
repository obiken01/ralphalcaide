﻿using System.Collections.Generic;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public interface IStatementLineManager
    {
        IList<StatementLine> GetList();
        bool AddStatementLines(IList<StatementLine> statementLines, BankStatement bankStatement);
        bool DeleteStatementLines(BankStatement bankStatement);
        bool DeleteStatementLines(IList<BankStatement> bankStatements);
        bool AddStatementLines(IList<StatementLine> statementLines);
    }
}