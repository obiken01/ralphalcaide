﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BnkStmntSearch.Database;
using log4net;
using DbBankStatement = BnkStmntSearch.Database.Model.BankStatement;
using EntityBankStatement = BnkStmntSearch.Data.Entities.BankStatement;
using System.Data.Entity;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public class BankStatementManager : IBankStatementManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<EntityBankStatement> GetList()
        {
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    return dbContext.BankStatement
                        .Include(i => i.FileInfo)
                        .Include(i => i.StatementLines)
                        .ToList().Select(i => new EntityBankStatement(i)).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool AddBankStatements(IList<EntityBankStatement> bankStatementList, FileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var dbFileInfo = dbContext.FileInfo.FirstOrDefault(i => i.FileName == fileInfo.FileName
                                                                            && i.UploadDate == fileInfo.UploadDate);
                    if (dbFileInfo == null)
                        throw new Exception($"File {fileInfo.FileName} was not found on the database.");
                    var dbBankStatementList = bankStatementList.Select(bankStatement => new DbBankStatement
                        {
                            LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime,
                            HPage = bankStatement.HPage,
                            GBasicHeader = bankStatement.GBasicHeader,
                            GApplication = bankStatement.GApplication,
                            Trn = bankStatement.Trn,
                            AccountIdentification = bankStatement.AccountIdentification,
                            StatementNo = bankStatement.StatementNo,
                            StatementSeqNo = bankStatement.StatementSeqNo,
                            OpenBalanceIndicator = bankStatement.OpenBalanceIndicator,
                            OpenBalanceDate = bankStatement.OpenBalanceDate,
                            OpenBalanceCurrency = bankStatement.OpenBalanceCurrency,
                            OpenBalanceAmount = bankStatement.OpenBalanceAmount,
                            StatementContent = bankStatement.StatementContent,
                            FileInfo = dbFileInfo,
                            Guid = bankStatement.Guid
                    })
                        .ToList();
                    dbContext.BankStatement.AddRange(dbBankStatementList);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool DeleteBankStatements(IList<BankStatement> bankStatementList)
        {
            if(bankStatementList == null)
                throw new ArgumentNullException(nameof(bankStatementList));

            try
            {
                var bankStatementIdList = bankStatementList.Select(i => i.Id).ToList();
                using (var dbContext = new DatabaseContext())
                {
                    var dbBankStatements = dbContext.BankStatement
                        .Where(i => bankStatementIdList.Contains(i.Id))
                        .ToList();
                    if (dbBankStatements.Count == 0)
                        throw new Exception("No bank statement records found.");
                    dbContext.BankStatement.RemoveRange(dbBankStatements);
                    dbContext.SaveChanges();
                    _logger.Info("Bank Statement has been deleted from the Database");
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public bool AddBankStatement(EntityBankStatement bankStatement, FileInfo fileInfo)
        {
            if (fileInfo == null)
                throw new ArgumentNullException(nameof(fileInfo));

            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var dbFileInfo = dbContext.FileInfo.FirstOrDefault(i => i.FileName == fileInfo.FileName
                                                                            && i.UploadDate == fileInfo.UploadDate);
                    if (dbFileInfo == null)
                        throw new Exception($"File {fileInfo.FileName} was not found on the database.");
                    var newBankStatement = new DbBankStatement
                    {
                        LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime,
                        HPage = bankStatement.HPage,
                        GBasicHeader = bankStatement.GBasicHeader,
                        GApplication = bankStatement.GApplication,
                        Trn = bankStatement.Trn,
                        AccountIdentification = bankStatement.AccountIdentification,
                        StatementNo = bankStatement.StatementNo,
                        StatementSeqNo = bankStatement.StatementSeqNo,
                        OpenBalanceIndicator = bankStatement.OpenBalanceIndicator,
                        OpenBalanceDate = bankStatement.OpenBalanceDate,
                        OpenBalanceCurrency = bankStatement.OpenBalanceCurrency,
                        OpenBalanceAmount = bankStatement.OpenBalanceAmount,
                        StatementContent = bankStatement.StatementContent,
                        FileInfo = dbFileInfo,
                        Guid = bankStatement.Guid
                    };
                    dbContext.BankStatement.Add(newBankStatement);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool AddBankStatementComplete(EntityBankStatement bankStatement, FileInfo fileInfo)
        {
            if (bankStatement == null)
                throw new ArgumentNullException(nameof(bankStatement));

            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var dbFileInfo = dbContext.FileInfo.FirstOrDefault(i => i.FileName == fileInfo.FileName
                                                                            && i.UploadDate == fileInfo.UploadDate);
                    if (dbFileInfo == null)
                        throw new Exception($"File {fileInfo.FileName} was not found on the database.");
                    var newBankStatement = new DbBankStatement
                    {
                        LogicalTerminalDateTime = bankStatement.LogicalTerminalDateTime,
                        HPage = bankStatement.HPage,
                        GBasicHeader = bankStatement.GBasicHeader,
                        GApplication = bankStatement.GApplication,
                        Trn = bankStatement.Trn,
                        AccountIdentification = bankStatement.AccountIdentification,
                        StatementNo = bankStatement.StatementNo,
                        StatementSeqNo = bankStatement.StatementSeqNo,
                        OpenBalanceIndicator = bankStatement.OpenBalanceIndicator,
                        OpenBalanceDate = bankStatement.OpenBalanceDate,
                        OpenBalanceCurrency = bankStatement.OpenBalanceCurrency,
                        OpenBalanceAmount = bankStatement.OpenBalanceAmount,
                        StatementContent = bankStatement.StatementContent,
                        Guid = bankStatement.Guid
                    };
                    dbFileInfo.BankStatements.Add(newBankStatement);
                    dbContext.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }
    }
}
