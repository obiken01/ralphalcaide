﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using BnkStmntSearch.Database;
using DbUploadHistory = BnkStmntSearch.Database.Model.UploadHistory;
using UploadHistoryEntity = BnkStmntSearch.Data.Entities.UploadHistory;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public class UploadHistoryManager : IUploadHistoryManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<UploadHistoryEntity> GetList()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.UploadHistory.ToList().Select(i => new UploadHistoryEntity(i)).ToList();
            }
        }

        public bool AddUploadHistory(string fileName, string uploadDate, string uploadBy, string dateDeleted)
        {
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var newUploadHistory = new DbUploadHistory
                    {
                        FileName = fileName,
                        UploadDate = uploadDate,
                        UploadedBy = uploadBy
                    };
                    dbContext.UploadHistory.Add(newUploadHistory);
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool AddUploadHistory(UploadHistoryEntity uploadHistory, FileInfo fileInfo)
        {
            return AddUploadHistory(uploadHistory.FileName, uploadHistory.UploadDate,
                uploadHistory.UploadedBy, string.Empty);
        }
    }
}
