﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using log4net;
using BnkStmntSearch.Database;
using DataUserRole = BnkStmntSearch.Data.Enum.UserRole;
using DatabaseUserRole = BnkStmntSearch.Database.Enum.UserRole;
using DatabaseUser = BnkStmntSearch.Database.Model.User;
using DataUser = BnkStmntSearch.Data.Entities.User;

namespace BnkStmntSearch.Data.Entities.Manager
{
    public class UserManager : IUserManager
    {
        private static readonly byte[] AesKey = Encoding.ASCII.GetBytes("RAADStNFBgcICQoLDA0ODw==");
        private static readonly byte[] AesIv = Encoding.ASCII.GetBytes("RT$2pIjRT$2pIj12");
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<DataUser> GetList()
        {
            using (var dbContext = new DatabaseContext())
            {
                return dbContext.User.ToList().Select(user => new DataUser(user)).ToList();
            }
        }

        public DataUser GetUserById(long id)
        {
            return GetList().SingleOrDefault(user => user.Id == id);
        }

        public DataUser GetUser(string userId, DataUserRole role)
        {
            return GetList().SingleOrDefault(user => user.UserId == userId && user.Role == role);
        }

        public bool AddUser(string userId, string password, DataUserRole role)
        {
            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var newUser = new DatabaseUser
                    {
                        UserId = userId,
                        Password = AesEncryptString(password),
                        Role = (DatabaseUserRole)role
                    };
                    dbContext.User.Add(newUser);
                    dbContext.SaveChanges();
                }
                _logger.Info($"{userId} is successfuly saved to the Database");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool UpdateUser(DataUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            try
            {
                using (var dbContext = new DatabaseContext())
                {
                    var dbUser = dbContext.User.SingleOrDefault(i => i.Id == user.Id);
                    if (dbUser == null)
                        throw new Exception($"User {user.Id}|{user.UserId} was not found from the database.");

                    dbUser.Password = user.Password;
                    dbUser.UserId = user.UserId;
                    dbUser.Role = (DatabaseUserRole)user.Role;
                    dbContext.SaveChanges();
                }

                _logger.Info($"{user.Id}|{user.UserId} is successfuly updated");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool DeleteUser(DataUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            try
            {
                var userId = user.UserId;
                using (var dbContext = new DatabaseContext())
                {
                    var dbUser = dbContext.User.SingleOrDefault(i => i.Id == user.Id);
                    if (dbUser == null)
                        throw new Exception($"User {user.Id}|{user.UserId} was not found from the database.");
                    dbContext.User.Remove(dbUser);
                    dbContext.SaveChanges();
                }
                _logger.Info($"{userId} was deleted from the Database");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public DataUser LoginUser(string userId, string password, DataUserRole role)
        {
            return GetList()
                .FirstOrDefault(user =>
                    user.UserId == userId &&
                    user.Password == AesEncryptString(password) &&
                    user.Role == role);
        }

        public static string AesEncryptString(string plainText)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException(nameof(plainText));

            using (var aesCryptoServiceProvider = new AesCryptoServiceProvider())
            {
                aesCryptoServiceProvider.Key = AesKey;
                aesCryptoServiceProvider.IV = AesIv;

                var encryptor =
                    aesCryptoServiceProvider.CreateEncryptor(aesCryptoServiceProvider.Key, aesCryptoServiceProvider.IV);

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                    var writer = new StreamWriter(cryptoStream);
                    writer.Write(plainText);
                    writer.Flush();
                    cryptoStream.FlushFinalBlock();
                    writer.Flush();
                    return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                }
            }
        }

        public static string AesDecryptString(string encryptedString)
        {
            if (string.IsNullOrEmpty(encryptedString))
                throw new ArgumentNullException(nameof(encryptedString));

            using (var aesCryptoServiceProvider = new AesCryptoServiceProvider())
            {
                aesCryptoServiceProvider.Key = AesKey;
                aesCryptoServiceProvider.IV = AesIv;

                var encryptor =
                    aesCryptoServiceProvider.CreateDecryptor(aesCryptoServiceProvider.Key, aesCryptoServiceProvider.IV);

                using (var memoryStream = new MemoryStream(Convert.FromBase64String(encryptedString)))
                {
                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Read);
                    var reader = new StreamReader(cryptoStream);
                    return reader.ReadToEnd();
                }
            }
        }

        public static DataUserRole GetUserRole(string role)
        {
            switch (role.ToLower())
            {
                case "staff":
                    return DataUserRole.Staff;
                case "officer":
                    return DataUserRole.Officer;
                case "power admin":
                    return DataUserRole.PowerAdmin;
                default:
                    return DataUserRole.NotSet;
            }
        }
    }
}
