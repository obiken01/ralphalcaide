﻿using System;

namespace BnkStmntSearch.Data.Entity
{
    public class SearchResult
    {
        public SearchResult(string id, string fileName, DateTime uploadDate, string content, string appNum, string bank)
        {
            Id = id;
            FileName = fileName;
            UploadDate = uploadDate;
            Content = content;
            ApplicationNumber = appNum;
            Bank = bank;
        }

        public string ApplicationNumber { get; }
        public string Id { get; }
        public string FileName { get; }
        public DateTime UploadDate { get; }
        public string Bank { get; }
        public string Content { get; }
    }
}