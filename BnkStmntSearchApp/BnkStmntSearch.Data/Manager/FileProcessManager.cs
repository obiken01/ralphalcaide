﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
//using BnkStmntSearch.NHibernate;
//using BnkStmntSearch.NHibernate.Repo;
using log4net;
using log4net.Util;
//using FileInfo = BnkStmntSearch.NHibernate.Repo.FileInfo;
using FileInfo = BnkStmntSearch.Data.Entities.FileInfo;
using User = BnkStmntSearch.Data.Entities.User;
using BnkStmntSearch.Data.Entities;
using BnkStmntSearch.Data.Entities.Manager;
using NHibernate.Util;

namespace BnkStmntSearch.Data.Manager
{
    public class FileProcessManager
    {
        private readonly User _currentUser;
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IFileInfoManager _fileInfoManager;
        private readonly IBankStatementManager _bankStatementManager;
        private readonly IStatementLineManager _statementLineManager;
        private readonly IUploadHistoryManager _uploadHistoryManager;
        public const string DateTimeFormat = "MM-dd-yyyy hh:mm:ss tt";

        public FileProcessManager(User user)
        {
            _currentUser = user;
            _fileInfoManager = new FileInfoManager();
            _bankStatementManager = new Entities.Manager.BankStatementManager();
            _statementLineManager = new StatementLineManager();
            _uploadHistoryManager = new Entities.Manager.UploadHistoryManager();
        }

        public bool FileUpload(string fileLocation)
        {
            bool isSuccessful;
            var startTime = DateTime.Now;
            try
            {
                var file = File.ReadAllBytes(fileLocation);
                var fileContent = File.ReadAllLines(fileLocation);
                var fileName = Path.GetFileName(fileLocation);

                /*using (var scope = NHSession.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        var fileInfo = new FileInfo
                        {
                            FileName = fileName,
                            UploadDate = DateTime.Now,
                            File = file,
                            UploadedBy = _currentUser.UserId
                        };
                        var processedFile = ProcessFileContent(fileContent, fileInfo);
                        fileInfo.BankStatements = processedFile ?? throw new Exception("File Upload Failed");
                        scope.Save(fileInfo);
                        var uploadHistory = new UploadHistory
                        {
                            FileName = fileInfo.FileName,
                            UploadDate = fileInfo.UploadDate,
                            UploadedBy = fileInfo.UploadedBy
                        };
                        scope.Save(uploadHistory);
                        transaction.Commit();
                    }
                }*/
                _logger.Info($"Started processing contents of {fileName}");
                var fileInfo = new FileInfo
                {
                    FileName = fileName,
                    UploadDate = DateTime.Now.ToString(DateTimeFormat),
                    File = file,
                    UploadedBy = _currentUser.UserId
                };
                var processedFile = ProcessFileContent(fileContent, fileInfo);
                fileInfo.BankStatements = processedFile ?? throw new Exception("File Upload Failed");
                _logger.Info($"Finished processing contents of {fileName}. ET= {(DateTime.Now - startTime)}");

                _logger.Info($"Started saving content to the database of {fileName}");
                _fileInfoManager.AddFullFileInfo(fileInfo);
                //_fileInfoManager.AddFileInfo(fileInfo);
                //_bankStatementManager.AddBankStatements(processedFile, fileInfo);
                //_statementLineManager.AddStatementLines(PopulateStatementLines(processedFile));
                var uploadHistory = new UploadHistory
                {
                    FileName = fileInfo.FileName,
                    UploadDate = fileInfo.UploadDate,
                    UploadedBy = fileInfo.UploadedBy
                };
                _uploadHistoryManager.AddUploadHistory(uploadHistory, fileInfo);
                BankStatementListManager.Instance.UpdateList();

                _logger.Info($"Finished saving content to the database of {fileName}. ET= {(DateTime.Now - startTime)}");
                _logger.Info($"{fileName} is successfuly saved to the Database");
                isSuccessful = true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                isSuccessful = false;
            }
            finally
            {
                var duration = DateTime.Now - startTime;
                _logger.Info($"Process Duration: {duration}");
            }

            return isSuccessful;
        }

        private IList<StatementLine> PopulateStatementLines(IList<BankStatement> bankStatements)
        {
            var result = new List<StatementLine>();
            foreach (var bankStatement in bankStatements)
            {
                bankStatement.StatementLines.ForEach(i => i.BankStatement = bankStatement);
                result.AddRange(bankStatement.StatementLines);
            }
            return result;
        }

        public IList<BankStatement> ProcessFileContent(string[] fileContent, FileInfo fileInfo)
        {
            try
            {
                StatementLine stmntLine = null, currStmntLine = null;
                bool isAtOpeningBalance = false,
                    isAtHStatement = false,
                    isARecur = false,
                    isAtClosing = false,
                    isAtRefOwnerServer = false;
                var bankStatement = new BankStatement {FileInfo = fileInfo, Guid = Guid.NewGuid().ToString()};
                var bankStatementList = new List<BankStatement>();
                var currentStatementSeq = string.Empty;
                var stringBuilder = new StringBuilder();
                var stringBuilder2 = new StringBuilder();
                var bank = new StringBuilder();
                var isSendBankNumFound = false;
                var lines = fileContent.Where(x => x.Trim() != string.Empty && x != " ").ToList();
                var fileType = GetFileType(lines[0]);
                foreach (var line in lines)
                {
                    stringBuilder.AppendLine(line.TrimEnd());
                    var trimmedColumns = line.Split(' ').Where(x => x != string.Empty).ToList();

                    if (trimmedColumns.Count == 0)
                        continue;

                    switch (fileType)
                    {
                        case FileType.Type1:
                            FileType1BankStatement(fileInfo, trimmedColumns, ref bankStatement, line,
                                bankStatementList, ref isAtOpeningBalance, ref stmntLine, ref currentStatementSeq,
                                ref isAtHStatement, ref isAtRefOwnerServer, ref stringBuilder, ref isARecur, ref isAtClosing);
                            break;
                        case FileType.Type2: // bug here
                            FileType2BankStatement(fileInfo, trimmedColumns, line, bankStatementList,
                                ref bankStatement, ref stringBuilder);
                            break;
                        case FileType.Type3: // bug here
                            FileType3BankStatement(fileInfo, trimmedColumns, line, bankStatementList,
                                ref bankStatement, ref stringBuilder2, ref bank, ref isSendBankNumFound);
                            stringBuilder2.AppendLine(line.TrimEnd());
                            break;
                        default:
                            throw new Exception("File Type Not Found");
                    }
                }
                return bankStatementList;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return null;
            }
        }

        private static FileType GetFileType(string line)
        {
            var trimmedColumns = line.Split(' ').Where(x => x != string.Empty).ToList();

            if (trimmedColumns[0].Contains("010040018"))
                return FileType.Type2;
            
            if (IsLineLogicalTerminal(trimmedColumns))
                return FileType.Type1;

            return FileType.Type3;// bug

        }

        private static void FileType2BankStatement(FileInfo fileInfo, List<string> trimmedColumns, string line,
            List<BankStatement> bankStatementList, ref BankStatement bankStatement, ref StringBuilder stringBuilder)
        {
            if (trimmedColumns.GetStringValue(0) == "010040018")
            {
                var stmntLine = new StatementLine
                {
                    ApplicationNumber = trimmedColumns.GetStringValue(2),
                    BankStatement = bankStatement
                };
                bankStatement.StatementLines.Add(stmntLine);
                return;
            }

            if (line.Contains("TRAN CODE"))
            {
                bankStatement.StatementContent = stringBuilder.ToString();
                stringBuilder = new StringBuilder();
                bankStatementList.Add(bankStatement);
                bankStatement = new BankStatement {FileInfo = fileInfo, Guid = Guid.NewGuid().ToString()};
            }
        }

        private static void FileType3BankStatement(FileInfo fileInfo, List<string> trimmedColumns, string line,
            List<BankStatement> bankStatementList, ref BankStatement bankStatement, ref StringBuilder stringBuilder,
            ref StringBuilder bank, ref bool isSendBankNumFound)
        {
            if (line.StartsWith("*******************************************")
                && isSendBankNumFound)
            {
                bank.AppendLine(line);
                isSendBankNumFound = false;
            }
            if (isSendBankNumFound)
            {
                bank.AppendLine(line);
            }
            if (line.StartsWith("*******************************************")
                && bank.Length == 0)
            {
                bank.AppendLine(line);
                isSendBankNumFound = true;
            }

            if (!line.Contains("***********") && line.StartsWith("*") || line.Contains("END OF REPORT"))
            {
                var lines = Regex.Split(stringBuilder.ToString(), "\r\n");
                var stmntLine = new StatementLine
                {
                    ApplicationNumber = lines[0] + "\r\n" + lines[1],
                    ChequeNo = bank.ToString(),
                    BankStatement = bankStatement
                };
                bankStatement.StatementLines.Add(stmntLine);
                bankStatement.StatementContent = stringBuilder.ToString();
                stringBuilder = new StringBuilder();
                bankStatementList.Add(bankStatement);
                bankStatement = new BankStatement {FileInfo = fileInfo, Guid = Guid.NewGuid().ToString()};
            }
        }

        private static void FileType1BankStatement(FileInfo fileInfo, List<string> trimmedColumns,
            ref BankStatement bankStatement, string line, List<BankStatement> bankStatementList,
            ref bool isAtOpeningBalance, ref StatementLine stmntLine, ref string currentStatementSeq,
            ref bool isAtHStatement, ref bool isAtRefOwnerServer, ref StringBuilder stringBuilder, 
            ref bool isARecur, ref bool isAtClosing)
        {
            if (IsLineLogicalTerminal(trimmedColumns))
            {
                bankStatement.LogicalTerminalDateTime = trimmedColumns.Count == 6
                    ? $"{trimmedColumns.GetStringValue(1)} {trimmedColumns.GetStringValue(2)}"
                    : $"{trimmedColumns.GetStringValue(0)} {trimmedColumns.GetStringValue(1)}";
                return;
            }

            if (trimmedColumns.Contains("HPage"))
            {
                bankStatement.HPage = trimmedColumns.GetStringValue(trimmedColumns.Count - 1);
                return;
            }

            if (line.Contains("Func PRTEXB") ||
                line.Contains("Message not to be authenticated") ||
                line.Contains("Further Reference / Information"))
                return;

            if (line.Contains("GBasic Header"))
            {
                var gBasicHeader = new StringBuilder();
                for (var index = 2; index < trimmedColumns.Count; index++)
                    gBasicHeader.Append($"{trimmedColumns.GetStringValue(index)} ");
                bankStatement.GBasicHeader = gBasicHeader.ToString();
                return;
            }

            if (line.Contains("GApplication Header"))
            {
                var gAppHeader = new StringBuilder();
                for (var index = 2; index < trimmedColumns.Count; index++)
                    gAppHeader.Append($"{trimmedColumns.GetStringValue(index)} ");
                bankStatement.GApplication = gAppHeader.ToString();
                return;
            }

            if (line.Contains("TRN"))
            {
                bankStatement.Trn = trimmedColumns.GetStringValue(3);
                return;
            }

            if (line.Contains("Account Ident."))
            {
                bankStatement.AccountIdentification = trimmedColumns.GetStringValue(4);
                return;
            }

            if (line.Contains("Statement / Sequence"))
            {
                bankStatement.StatementNo = trimmedColumns.GetStringValue(6);
                bankStatement.StatementSeqNo = trimmedColumns.GetStringValue(7);
                return;
            }

            if (line.Contains("Opening Balance"))
            {
                isAtOpeningBalance = true;
                return;
            }

            if (isAtOpeningBalance)
            {
                bankStatement.OpenBalanceIndicator = trimmedColumns.GetStringValue(3);
                bankStatement.OpenBalanceDate = trimmedColumns.GetStringValue(4);
                bankStatement.OpenBalanceCurrency = trimmedColumns.GetStringValue(5);
                bankStatement.OpenBalanceAmount = trimmedColumns.GetStringValue(6);
                isAtOpeningBalance = false;
                return;
            }

            if (line.Contains("Repeatable Sequence"))
            {
                var seq = trimmedColumns.GetStringValue(trimmedColumns.Count - 1);
                if (stmntLine == null )
                {
                    stmntLine = new StatementLine {Sequence = seq, BankStatement = bankStatement};
                    currentStatementSeq = stmntLine.Sequence;
                }
                else if (seq != currentStatementSeq && !isAtClosing)
                {
                    if(!string.IsNullOrEmpty(stmntLine.ApplicationNumber))
                        bankStatement.StatementLines.Add(stmntLine);
                    stmntLine = new StatementLine {Sequence = seq, BankStatement = bankStatement};
                    currentStatementSeq = stmntLine.Sequence;
                }
                isARecur = true;
                return;
            }

            if (line.Contains("Statement Line"))
            {
                isAtHStatement = true;
                return;
            }

            if (line.Contains("Closing Balance"))
            {
                if (!string.IsNullOrEmpty(stmntLine.ApplicationNumber))
                    bankStatement.StatementLines.Add(stmntLine);
                stmntLine = null;
                currentStatementSeq = string.Empty;
                isAtClosing = true;
                isARecur = false;
                return;
            }

            if (isAtHStatement)
            {
                stmntLine.Date = trimmedColumns.GetStringValue(2);
                stmntLine.Amount = trimmedColumns.GetStringValue(5);
                isAtHStatement = false;
                return;
            }

            if (line.Contains("Ref. Owner") && !isAtClosing)
            {
                isAtRefOwnerServer = true;
                return;
            }

            if (isAtRefOwnerServer && !isAtClosing)
            {
                stmntLine.ApplicationNumber = trimmedColumns.GetStringValue(0);
                stmntLine.RefServer = trimmedColumns.GetStringValue(2);
                isAtRefOwnerServer = false;
                return;
            }

            if (line.Contains("CHEQUE NO"))
            {
                stmntLine.ChequeNo = trimmedColumns.GetStringValue(2);
                bankStatement.StatementLines.Add(stmntLine);
                stmntLine = null;
                currentStatementSeq = string.Empty;
                return;
            }

            if (line.Contains("GTrailer"))
            {
                bankStatement.StatementContent = stringBuilder.ToString();
                stringBuilder = new StringBuilder();
                if(bankStatement.StatementLines.Count != 0)
                    bankStatementList.Add(bankStatement);
                bankStatement = new BankStatement {FileInfo = fileInfo, Guid = Guid.NewGuid().ToString() };
                stmntLine = new StatementLine { BankStatement = bankStatement };
                currentStatementSeq = null;
                isAtClosing = false;
            }
        }

        private static bool IsLineLogicalTerminal(ICollection<string> trimmedColumns)
        {
            return trimmedColumns.Contains("Logical") &&
                   trimmedColumns.Contains("Terminal") &&
                   trimmedColumns.Contains("1XHA");
        }
    }

    internal enum FileType
    {
        NotSet = 0,
        Type1 = 1,
        Type2 = 2,
        Type3 = 3
    }

    internal static class ListExtensionMethod
    {
        public static string GetStringValue(this List<string> list, int index)
        {
            try
            {
                return index > list.Count ? string.Empty : list[index];
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}