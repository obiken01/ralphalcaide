﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BnkStmntSearch.Data.Entities;
using BnkStmntSearch.Data.Entities.Manager;
using log4net;
using EntityFileInfo = BnkStmntSearch.Data.Entities.FileInfo;

namespace BnkStmntSearch.Data.Manager
{
    public class FileArchiveManager
    {
        private readonly string _arcvhiveFileDropLocation;
        private readonly User _currentUser;
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly int _maxDegreeOfParallelism;
        private readonly IFileInfoManager _fileInfoManager;
        private readonly IBankStatementManager _bankStatementManager;
        private readonly IStatementLineManager _statementLineManager;

        public FileArchiveManager(User user, string arcvhiveFileDropLocation)
        {
            _currentUser = user;
            _maxDegreeOfParallelism = Environment.ProcessorCount;
            _arcvhiveFileDropLocation = arcvhiveFileDropLocation;
            _fileInfoManager = new FileInfoManager();
            _bankStatementManager = new Entities.Manager.BankStatementManager();
            _statementLineManager = new StatementLineManager();
        }

        public IList<EntityFileInfo> GetList()
        {
            return _fileInfoManager.GetList();
        }

        public void ArchiveFiles()
        {
            var deductedDate = Subtract3WorkingDays(DateTime.Now);
            var syncObj = new object();
            Parallel.ForEach(
                GetList(),
                new ParallelOptions {MaxDegreeOfParallelism = _maxDegreeOfParallelism},
                file =>
                {
                    lock (syncObj)
                    {
                        if (file.GetUploadDate() >= deductedDate)
                            return;
                        try
                        {
                            var path = Path.Combine(_arcvhiveFileDropLocation,
                                $"{file.GetUploadDate():hhmmssddMMyyyy}-{file.FileName}");
                            File.WriteAllBytes(path, file.File);
                            _logger.Info(
                                $"Upload Date: {file.GetUploadDate().ToShortDateString()}| File: {file.FileName} has been archived.");
                            DeleteRecord(file);
                        }
                        catch (Exception e)
                        {
                            _logger.Error(e);
                        }
                    }
                });
        }

        private bool DeleteRecord(EntityFileInfo record)
        {
            try
            {
                var bankStatements = _bankStatementManager.GetList().Where(i => i.FileInfo.Id == record.Id).ToList();

                _statementLineManager.DeleteStatementLines(bankStatements);
                _bankStatementManager.DeleteBankStatements(bankStatements);
                _fileInfoManager.DeleteFileInfo(record);
                BankStatementListManager.Instance.UpdateList();
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public static DateTime Subtract3WorkingDays(DateTime dateTime)
        {
            var result = dateTime.AddDays(-3);
            if (result.DayOfWeek == DayOfWeek.Saturday)
                result = result.AddDays(-1);
            if (result.DayOfWeek == DayOfWeek.Sunday)
                result = result.AddDays(-2);
            return result;
        }
    }
}