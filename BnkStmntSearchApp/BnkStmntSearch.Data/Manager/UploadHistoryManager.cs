﻿using System.Collections.Generic;
using BnkStmntSearch.NHibernate;
using BnkStmntSearch.NHibernate.Repo;

namespace BnkStmntSearch.Data.Manager
{
    public class UploadHistoryManager
    {
        public IList<UploadHistory> GetList()
        {
            using (var scope = NHSession.OpenSession())
            {
                return scope.QueryOver<UploadHistory>().List();
            }
        }
    }
}