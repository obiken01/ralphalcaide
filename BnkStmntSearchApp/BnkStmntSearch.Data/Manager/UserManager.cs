﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using BnkStmntSearch.NHibernate;
using BnkStmntSearch.NHibernate.Enum;
using BnkStmntSearch.NHibernate.Repo;
using log4net;

namespace BnkStmntSearch.Data.Manager
{
    public class UserManager
    {
        private static readonly byte[] AesKey = Encoding.ASCII.GetBytes("RAADStNFBgcICQoLDA0ODw==");
        private static readonly byte[] AesIv = Encoding.ASCII.GetBytes("RT$2pIjRT$2pIj12");
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<User> GetList()
        {
            using (var scope = NHSession.OpenSession())
            {
                return scope.QueryOver<User>().List();
            }
        }

        public User GetUserById(long id)
        {
            return GetList().SingleOrDefault(i => i.Id == id);
        }

        public User GetUser(string userId, UserRole role)
        {
            return GetList().SingleOrDefault(user => user.UserId == userId && user.Role == role);
        }

        public bool AddUser(string userId, string password, UserRole role)
        {
            try
            {
                using (var scope = NHSession.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        var newUser = new User
                        {
                            UserId = userId,
                            Password = AesEncryptString(password),
                            Role = role
                        };
                        scope.Save(newUser);
                        transaction.Commit();
                    }
                }
                _logger.Info($"{userId} is successfuly saved to the Database");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            try
            {
                using (var scope = NHSession.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        scope.SaveOrUpdate(user);
                        transaction.Commit();
                    }
                }
                _logger.Info($"{user.Id}|{user.UserId} is successfuly updated");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public bool DeleteUser(User user)
        {
            if (user == null) return false;
            var userId = user.UserId;
            try
            {
                using (var scope = NHSession.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        scope.Delete(user);
                        transaction.Commit();
                    }
                }
                _logger.Info($"{userId} was deleted from the Database");
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e);
                return false;
            }
        }

        public User LoginUser(string userId, string password, UserRole role)
        {
            foreach (var user in GetList())
                if (user.UserId == userId &&
                    user.Password == AesEncryptString(password) &&
                    user.Role == role)
                    return user;
            return null;
        }

        public static string AesEncryptString(string plainText)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException(nameof(plainText));

            using (var aesCryptoServiceProvider = new AesCryptoServiceProvider())
            {
                aesCryptoServiceProvider.Key = AesKey;
                aesCryptoServiceProvider.IV = AesIv;

                var encryptor =
                    aesCryptoServiceProvider.CreateEncryptor(aesCryptoServiceProvider.Key, aesCryptoServiceProvider.IV);

                using (var memoryStream = new MemoryStream())
                {
                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                    var writer = new StreamWriter(cryptoStream);
                    writer.Write(plainText);
                    writer.Flush();
                    cryptoStream.FlushFinalBlock();
                    writer.Flush();
                    return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int) memoryStream.Length);
                }
            }
        }

        public static string AesDecryptString(string encryptedString)
        {
            if (string.IsNullOrEmpty(encryptedString))
                throw new ArgumentNullException(nameof(encryptedString));

            using (var aesCryptoServiceProvider = new AesCryptoServiceProvider())
            {
                aesCryptoServiceProvider.Key = AesKey;
                aesCryptoServiceProvider.IV = AesIv;

                var encryptor =
                    aesCryptoServiceProvider.CreateDecryptor(aesCryptoServiceProvider.Key, aesCryptoServiceProvider.IV);

                using (var memoryStream = new MemoryStream(Convert.FromBase64String(encryptedString)))
                {
                    var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Read);
                    var reader = new StreamReader(cryptoStream);
                    return reader.ReadToEnd();
                }
            }
        }

        public static UserRole GetUserRole(string role)
        {
            switch (role.ToLower())
            {
                case "staff":
                    return UserRole.Staff;
                case "officer":
                    return UserRole.Officer;
                case "power admin":
                    return UserRole.PowerAdmin;
                default:
                    return UserRole.NotSet;
            }
        }
    }
}