﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using BnkStmntSearch.Data.Entities.Manager;
using BnkStmntSearch.Data.Entities;
using SearchResult = BnkStmntSearch.Data.Entity.SearchResult;
using log4net;

namespace BnkStmntSearch.Data.Manager
{
    public class SearchBankStatementManager
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IList<BankStatement> GetList()
        {
            try
            {
                return BankStatementListManager.Instance.GetList();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        public IList<BankStatement> SearchByApplicationNumber(string applicationNumber)
        {
            return (from bankStatement in GetList()
                let isIncluded =
                bankStatement.StatementLines.Any(
                    statementLine => statementLine.ApplicationNumber == applicationNumber)
                where isIncluded
                select bankStatement).ToList();
        }

        public IList<SearchResult> SearchResultByApplicationNumber(string applicationNumber, bool isExtact = false)
        {
            if(applicationNumber == string.Empty) return new List<SearchResult>();
            var result = new List<SearchResult>();
            foreach (var bankStatement in GetList())
            {
                if (IsSameAppNumber(bankStatement.StatementContent, applicationNumber, isExtact))
                {
                    result.Add(new SearchResult($"{bankStatement.Id}",
                        bankStatement.FileInfo.FileName,
                        bankStatement.FileInfo.GetUploadDate(),
                        //bankStatement.StatementContent,
                        ProcessContent(bankStatement.StatementContent, applicationNumber, out string bank, bankStatement),
                        applicationNumber,
                        bank));
                }
                else
                {
                    result.Add(new SearchResult("0",
                        string.Empty,
                        DateTime.Now,
                        string.Empty,
                        string.Empty,
                        string.Empty));
                }

            }
            return result;
        }

        public IList<SearchResult> SearchResultByApplicationNumber(IList<string> applicationNumbers, bool isExtact = false)
        {
            if(applicationNumbers == null || applicationNumbers.Count == 0) return new List<SearchResult>();
            var result = new List<SearchResult>();
            foreach (var bankStatement in GetList())
            {
                var isRecordFound = false;
                foreach (var applicationNumber in applicationNumbers)
                {
                    if (IsSameAppNumber(bankStatement.StatementContent, applicationNumber, isExtact))
                    {
                        result.Add(new SearchResult($"{bankStatement.Id}",
                            bankStatement.FileInfo.FileName,
                            bankStatement.FileInfo.GetUploadDate(),
                            //bankStatement.StatementContent,
                            ProcessContent(bankStatement.StatementContent, applicationNumber, out string bank, bankStatement),
                            applicationNumber,
                            bank));
                        isRecordFound = true;
                    }
                }
                if (!isRecordFound)
                {
                    foreach (var statementLine in bankStatement.StatementLines)
                    {
                        result.Add(new SearchResult("0",
                            string.Empty,
                            DateTime.Now,
                            string.Empty,
                            statementLine.ApplicationNumber,
                            string.Empty));
                    }
                }
            }
            return result;
        }

        private string ProcessContent(string wholeContent, string appNum, 
            out string bank, BankStatement bankStatement)
        {
            if (wholeContent.StartsWith("010040018"))
            {
                bank = string.Empty;
                return wholeContent;
            }
            if (wholeContent.StartsWith("*MISC CREDIT")
                || wholeContent.StartsWith("*BOOK TRANSFER"))
            {
                bank = bankStatement?.StatementLines[0]?.ChequeNo;
                return wholeContent;
            }
            var isSendingBank = false;
            var content = new StringBuilder();
            var bankcontent = new StringBuilder();
            var appContent = new StringBuilder();
            var appContentList = new List<string>();
            var lastRepeatableSequence = string.Empty;
            var contentLine = 0;
            foreach (var line in wholeContent.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None))
            {
                if (line.Contains("Logical Terminal 1XHA") && !isSendingBank && string.IsNullOrEmpty(bankcontent.ToString()))
                    isSendingBank = true;
                if (isSendingBank && line.Contains("GUser Header"))
                {
                    bankcontent.AppendLine(line);
                    isSendingBank = false;
                    continue;
                }
                else if (isSendingBank)
                {
                    bankcontent.AppendLine(line);
                    continue;
                }

                if (line.Contains("Repeatable Sequence"))
                {
                    lastRepeatableSequence = line;
                    continue;
                }
                if (line.Contains("Statement Line") && contentLine == 0)
                {
                    appContent.AppendLine(lastRepeatableSequence);
                    appContent.AppendLine(line);
                    contentLine++;
                    continue;
                }
                if (contentLine > 0 && contentLine < 6)
                {
                    appContent.AppendLine(line);
                    contentLine++;
                    if (contentLine == 6)
                    {
                        appContentList.Add(appContent.ToString());
                        appContent = new StringBuilder();
                        lastRepeatableSequence = string.Empty;
                        contentLine = 0;
                    }
                    continue;
                }
            }
            foreach (var appContentItem in appContentList)
            {
                if (!appContentItem.Contains(appNum)) continue;
                content.AppendLine(appContentItem);
                break;
            }
            bank = bankcontent.ToString();
            return content.ToString();
        }

        private static bool IsSameAppNumber(string applicationNumber, string searchValue,bool isExtact)
        {
            if (isExtact)
                return applicationNumber == searchValue;
            return !string.IsNullOrEmpty(applicationNumber) && applicationNumber.Contains(searchValue);

            //return isExtact && !string.IsNullOrEmpty(applicationNumber) ? applicationNumber == searchValue : applicationNumber.Contains(searchValue);
        }
    }
}