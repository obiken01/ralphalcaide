﻿using BnkStmntSearch.NHibernate.Map;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace BnkStmntSearchApp.Test.NHibernate
{
    internal class NHSessionTest
    {
        public static ISession OpenSession()
        {
            var connStr =
                @"Server=.\sqlexpress;Database=BnkStmntSearchDb_base;Integrated Security=false;user id=sa;password=sql;";
            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012
                    .ConnectionString(connStr)
                )
                .Mappings(m =>
                    m.FluentMappings
                        .AddFromAssemblyOf<UserRepoMap>()
                        .AddFromAssemblyOf<FileInfoRepoMap>()
                        .AddFromAssemblyOf<StatementLineRepoMap>()
                        .AddFromAssemblyOf<BankStatementRepoMap>())
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                .BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}