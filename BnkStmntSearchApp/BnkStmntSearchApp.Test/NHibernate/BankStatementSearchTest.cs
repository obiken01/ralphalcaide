﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using BnkStmntSearch.NHibernate.Repo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BnkStmntSearchApp.Test.NHibernate
{
    [TestClass]
    public class BankStatementSearchTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            //00000038742
            //var mgr = new BankStatementManager();
            var result = SearchByApplicationNumber("00000038742");

            Debug.WriteLine(result.Count);
            foreach (var bankStatement in result)
            {
                var text = TrimContent(bankStatement.StatementContent.Trim());
                Debug.WriteLine(text);
            }
        }

        private string TrimContent(string content)
        {
            var strBldr = new StringBuilder();
            //return Regex.Replace(content, @"[^\u0000-\u0070F]+", string.Empty);
            return Regex.Replace(content, @"[^a-zA-Z0-9/*\\:_\- \r\n]", string.Empty);

            /*return Encoding.ASCII.GetString(
                Encoding.Convert(
                    Encoding.UTF8,
                    Encoding.GetEncoding(
                        Encoding.ASCII.EncodingName,
                        new EncoderReplacementFallback(string.Empty),
                        new DecoderExceptionFallback()
                    ),
                    Encoding.UTF8.GetBytes(content)));*/
        }

        public IList<BankStatement> SearchByApplicationNumber(string applicationNumber)
        {
            using (var scope = NHSessionTest.OpenSession())
            {
                return (from bankStatement in scope.QueryOver<BankStatement>().List()
                    let isIncluded =
                    bankStatement.StatementLines.Any(
                        statementLine => statementLine.ApplicationNumber == applicationNumber)
                    where isIncluded
                    select bankStatement).ToList();
            }
        }
    }
}