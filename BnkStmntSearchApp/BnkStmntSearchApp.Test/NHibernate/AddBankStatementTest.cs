﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BnkStmntSearch.Data.Manager;
using BnkStmntSearch.NHibernate;
using BnkStmntSearch.NHibernate.Repo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BnkStmntSearchApp.Test.NHibernate
{
    [TestClass]
    public class AddBankStatementTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var bnkStmnt = new BankStatement
            {
                AccountIdentification = "AccountIdentification",
                GApplication = "GApplication",
                GBasicHeader = "GBasicHeader",
                HPage = "HPage",
                OpenBalanceCurrency = "OpenBalanceCurrency",
                StatementContent = "StatementContent"
            };
            var stmntLn = new StatementLine
            {
                Sequence = "001",
                Amount = "Amount",
                ApplicationNumber = "ApplicationNumber",
                BankStatement = bnkStmnt
            };
            //bnkStmnt.StatementLines = new List<StatementLine> {stmntLn};
            bnkStmnt.StatementLines.Add(stmntLn);
            var bnkStmntManager = new SearchBankStatementManager();

            var list = GetList();
            
            //Assert.IsTrue(AddBankStatement(list[0]));
            Assert.IsTrue(AddBankStatement(bnkStmnt));
        }

        public IList<BankStatement> GetList()
        {
            using (var scope = NHSessionTest.OpenSession())
            {
                return scope.QueryOver<BankStatement>().List();
            }
        }

        public bool AddBankStatement(BankStatement bankStatement)
        {
            try
            {
                using (var scope = NHSessionTest.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        scope.Save(bankStatement);
                        transaction.Commit();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public bool DeleteBankStatement(BankStatement bankStatement)
        {
            try
            {
                using (var scope = NHSessionTest.OpenSession())
                {
                    using (var transaction = scope.BeginTransaction())
                    {
                        scope.Delete(bankStatement);
                        transaction.Commit();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }
    }
}