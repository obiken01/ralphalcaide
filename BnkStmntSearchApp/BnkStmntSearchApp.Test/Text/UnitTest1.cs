﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using BnkStmntSearch.Data.Manager;
using BnkStmntSearch.NHibernate;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BnkStmntSearchApp.Test.Text
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //FileUpload(@"C:\ralph-dev\test\10.20.2016a.txt");
            /*var date = new DateTime(2017, 07, 11);
            var result = Subtract3WorkingDays(date);
            Console.WriteLine(result);
            Console.WriteLine(result.DayOfWeek);*/
            Console.WriteLine(GetProductVersion());
        }

        public string GetProductVersion()
        {
            /*var attribute = (AssemblyVersionAttribute)Assembly
                .GetExecutingAssembly()
                .GetCustomAttributes(typeof(AssemblyVersionAttribute), true)
                .Single();*/

            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            return version.ToString();
        }

        public bool FileUpload(string fileLocation)
        {
            var isSuccessful = false;
            try
            {
                var fileBytes = File.ReadAllBytes(fileLocation);
                var fileContent = File.ReadAllLines(fileLocation);
                var fileName = Path.GetFileName(fileLocation);

                /*foreach (var line in fileContent)
                {
                    Debug.WriteLine(line);
                    Console.WriteLine(line);
                }*/
                var man = new FileProcessManager(null);
                Assert.IsNotNull(man.ProcessFileContent(fileContent,null));

                foreach (var content in fileContent.Where(x => x != string.Empty))
                {
                    var trimmedLines = content.Split(' ').Where(x => x != string.Empty);
                    var stringBuilder = new StringBuilder();
                    stringBuilder.Append($"{trimmedLines.Count()} => ");
                    foreach (var column in trimmedLines)
                    {
                        stringBuilder.Append($"{column}|");
                    }
                    Debug.WriteLine(stringBuilder.ToString());

                }
                /*

                                using (var file = new StreamWriter($@"C:\ralph-dev\test\{DateTime.Now.ToString("hhmmss")}-test.txt"))
                                {
                                    foreach (var content in fileContent.Where(x => x != string.Empty))
                                    {
                                        var trimmedLines = content.Split(' ').Where(x => x != string.Empty);
                                        var stringBuilder = new StringBuilder();
                                        foreach (var column in trimmedLines)
                                        {
                                            stringBuilder.Append($"{column}|");
                                        }
                                        file.WriteLine(stringBuilder.ToString());

                                    }
                                    file.Close();
                                }
                */

                isSuccessful = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                Console.WriteLine(e);
                isSuccessful = false;
            }

            return isSuccessful;
        }

        public static DateTime Subtract3WorkingDays(DateTime dateTime)
        {
            var result = dateTime.AddDays(-3);
            if (result.DayOfWeek == DayOfWeek.Saturday)
                result = result.AddDays(-1);
            if (result.DayOfWeek == DayOfWeek.Sunday)
                result = result.AddDays(-2);
            return result;
        }
    }
}
