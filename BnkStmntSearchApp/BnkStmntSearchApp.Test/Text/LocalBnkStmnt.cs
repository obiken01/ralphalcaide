﻿using System;
using System.Diagnostics;
using System.IO;
using BnkStmntSearch.Data.Manager;
using BnkStmntSearch.NHibernate.Repo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileInfo = BnkStmntSearch.NHibernate.Repo.FileInfo;

namespace BnkStmntSearchApp.Test.Text
{
    [TestClass]
    public class LocalBnkStmnt
    {
        [TestMethod]
        public void TestMethod1()
        {
            //FileUpload(@"C:\ralph-dev\test\10.20.2016a.txt");
            /*var date = new DateTime(2017, 07, 11);
            var result = Subtract3WorkingDays(date);
            Console.WriteLine(result);
            Console.WriteLine(result.DayOfWeek);*/
            var user = new User {UserId = "test"};
            var mgr = new FileProcessManager(user);

            const string fileLocation = @"C:\ralph-dev\test\local cover 06.09.17.txt";

            var file = File.ReadAllBytes(fileLocation);
            var fileContent = File.ReadAllLines(fileLocation);
            var fileName = Path.GetFileName(fileLocation);

            var fileInfo = new FileInfo
            {
                FileName = fileName,
                UploadDate = DateTime.Now,
                File = file,
                UploadedBy = user.UserId
            };

            var processedFile = mgr.ProcessFileContent(fileContent, fileInfo);
            if(processedFile == null)
                Assert.Fail("processedFile is null");
            foreach (var bankStatement in processedFile)
            {
                Debug.WriteLine(bankStatement.StatementLines[0].ApplicationNumber);
                Debug.WriteLine(bankStatement.StatementContent);
            }
        }
    }
}