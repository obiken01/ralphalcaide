﻿namespace StAta
{
    partial class Search
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.ApplicationNumberTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.SearchResultDetailsDataGrid = new System.Windows.Forms.DataGridView();
            this.ResId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResAppNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SendingBank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchResultDetailsDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1292, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.ApplicationNumberTextBox);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(10, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1454, 178);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Application Number:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1373, 148);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ApplicationNumberTextBox
            // 
            this.ApplicationNumberTextBox.Location = new System.Drawing.Point(6, 18);
            this.ApplicationNumberTextBox.Multiline = true;
            this.ApplicationNumberTextBox.Name = "ApplicationNumberTextBox";
            this.ApplicationNumberTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ApplicationNumberTextBox.Size = new System.Drawing.Size(1442, 124);
            this.ApplicationNumberTextBox.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.SearchResultDetailsDataGrid);
            this.groupBox4.Location = new System.Drawing.Point(10, 187);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1457, 420);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Result Details";
            // 
            // SearchResultDetailsDataGrid
            // 
            this.SearchResultDetailsDataGrid.AllowUserToAddRows = false;
            this.SearchResultDetailsDataGrid.AllowUserToDeleteRows = false;
            this.SearchResultDetailsDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SearchResultDetailsDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ResId,
            this.ResAppNum,
            this.ResultAmount,
            this.SendingBank});
            this.SearchResultDetailsDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchResultDetailsDataGrid.Location = new System.Drawing.Point(3, 18);
            this.SearchResultDetailsDataGrid.Name = "SearchResultDetailsDataGrid";
            this.SearchResultDetailsDataGrid.ReadOnly = true;
            this.SearchResultDetailsDataGrid.RowTemplate.Height = 24;
            this.SearchResultDetailsDataGrid.Size = new System.Drawing.Size(1451, 399);
            this.SearchResultDetailsDataGrid.TabIndex = 0;
            // 
            // ResId
            // 
            this.ResId.HeaderText = "Column1";
            this.ResId.Name = "ResId";
            this.ResId.ReadOnly = true;
            this.ResId.Visible = false;
            // 
            // ResAppNum
            // 
            this.ResAppNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ResAppNum.DefaultCellStyle = dataGridViewCellStyle1;
            this.ResAppNum.HeaderText = "App Num";
            this.ResAppNum.Name = "ResAppNum";
            this.ResAppNum.ReadOnly = true;
            this.ResAppNum.Width = 95;
            // 
            // ResultAmount
            // 
            this.ResultAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ResultAmount.DefaultCellStyle = dataGridViewCellStyle2;
            this.ResultAmount.HeaderText = "Amount";
            this.ResultAmount.Name = "ResultAmount";
            this.ResultAmount.ReadOnly = true;
            this.ResultAmount.Width = 85;
            // 
            // SendingBank
            // 
            this.SendingBank.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SendingBank.DefaultCellStyle = dataGridViewCellStyle3;
            this.SendingBank.HeaderText = "Send Bank";
            this.SendingBank.Name = "SendingBank";
            this.SendingBank.ReadOnly = true;
            this.SendingBank.Width = 106;
            // 
            // Search
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Name = "Search";
            this.Size = new System.Drawing.Size(1470, 610);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SearchResultDetailsDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox ApplicationNumberTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView SearchResultDetailsDataGrid;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResAppNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn SendingBank;
    }
}
