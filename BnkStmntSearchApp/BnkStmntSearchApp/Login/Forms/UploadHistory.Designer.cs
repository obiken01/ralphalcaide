﻿namespace BnkStmntSearchApp.Login.Forms
{
    partial class UploadHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UploadHistory));
            this.CloseBtn = new System.Windows.Forms.Button();
            this.uploadDataGrid = new System.Windows.Forms.DataGridView();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UploadBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UploadDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.uploadDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // CloseBtn
            // 
            this.CloseBtn.Location = new System.Drawing.Point(558, 318);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(96, 23);
            this.CloseBtn.TabIndex = 4;
            this.CloseBtn.Text = "Close";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // uploadDataGrid
            // 
            this.uploadDataGrid.AllowUserToAddRows = false;
            this.uploadDataGrid.AllowUserToDeleteRows = false;
            this.uploadDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.uploadDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileName,
            this.UploadBy,
            this.UploadDate});
            this.uploadDataGrid.Location = new System.Drawing.Point(12, 12);
            this.uploadDataGrid.Name = "uploadDataGrid";
            this.uploadDataGrid.ReadOnly = true;
            this.uploadDataGrid.RowTemplate.Height = 24;
            this.uploadDataGrid.Size = new System.Drawing.Size(642, 300);
            this.uploadDataGrid.TabIndex = 5;
            // 
            // FileName
            // 
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Width = 200;
            // 
            // UploadBy
            // 
            this.UploadBy.HeaderText = "Upload By";
            this.UploadBy.Name = "UploadBy";
            this.UploadBy.ReadOnly = true;
            // 
            // UploadDate
            // 
            this.UploadDate.HeaderText = "Upload Date";
            this.UploadDate.Name = "UploadDate";
            this.UploadDate.ReadOnly = true;
            // 
            // UploadHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 353);
            this.Controls.Add(this.uploadDataGrid);
            this.Controls.Add(this.CloseBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UploadHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upload History";
            ((System.ComponentModel.ISupportInitialize)(this.uploadDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.DataGridView uploadDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UploadBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn UploadDate;
    }
}