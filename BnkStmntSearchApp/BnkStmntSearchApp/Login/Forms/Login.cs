﻿using System;
using System.Reflection;
using System.Windows.Forms;
using BnkStmntSearch.Data.Entities.Manager;
using BnkStmntSearch.Data.Enum;
//using UserManager = BnkStmntSearch.Data.Manager.UserManager;

namespace BnkStmntSearchApp.Login.Forms
{
    public partial class Login : Form
    {
        public static LoggedUser CurrentUser = LoggedUser.Instance;
        private readonly IUserManager _userManager;

        public Login()
        {
            InitializeComponent();
            _userManager = new UserManager();
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            //LoginUser("power-admin", "P@ssword", UserRole.PowerAdmin); //todo: for testing only
            LoginUser(UserNameTextBox.Text, PasswordTextBox.Text, UserManager.GetUserRole(RoleComboBox.Text));
        }

        private void LoginUser(string userId, string password, UserRole role)
        {
            var user = _userManager.LoginUser(userId, password, role);
            if (user != null)
            {
                CurrentUser.Login(user);
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(this, "No user found.", "Login Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Login_FormClosing_1(object sender, FormClosingEventArgs e)
        {
        }
    }
}