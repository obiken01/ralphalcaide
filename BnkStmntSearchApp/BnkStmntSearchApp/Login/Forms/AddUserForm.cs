﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using BnkStmntSearch.Data.Manager;
using BnkStmntSearch.NHibernate.Enum;
using log4net;

namespace BnkStmntSearchApp.Login.Forms
{
    public partial class AddUserForm : Form
    {
        public static LoggedUser CurrentUser = LoggedUser.Instance;

        private ILog _logger;
        private readonly UserManager _userManager;

        public AddUserForm()
        {
            InitializeComponent();
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _userManager = new UserManager();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (!ValidateUserForm(out var message))
            {
                MessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (_userManager.AddUser(UserIdTextbox.Text, PwdTextbox.Text, UserManager.GetUserRole(RoleComboBox.Text)))
            {
                MessageBox.Show(this, $"{UserIdTextbox.Text} is successfuly saved", "Add User", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(this, "An error encountered on saving user to database.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ValidatePassword();
        }

        private void AddUserForm_Load(object sender, EventArgs e)
        {
            ValidatePassword();
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void ValidatePassword()
        {
            if (string.IsNullOrEmpty(PwdTextbox.Text) || string.IsNullOrEmpty(ConfPwdTextbox.Text))
                AddBtn.Enabled = false;
            AddBtn.Enabled = PwdTextbox.Text == ConfPwdTextbox.Text;
        }

        private bool CheckIfUserExist()
        {
            return _userManager.GetList().Any(
                user => user.UserId == UserIdTextbox.Text && 
                user.Role == UserManager.GetUserRole(RoleComboBox.Text));
        }

        private bool ValidateUserForm(out string message)
        {
            var messageStringBuilder = new StringBuilder();
            var isValid = true;

            if (UserManager.GetUserRole(RoleComboBox.Text) == UserRole.NotSet)
            {
                messageStringBuilder.Append("Entered role is not valid");
                isValid = false;
            }
            if (CheckIfUserExist())
            {
                messageStringBuilder.AppendLine("User already exist.");
                isValid = false;
            }

            message = isValid ? string.Empty : messageStringBuilder.ToString();
            return isValid;
        }
    }
}