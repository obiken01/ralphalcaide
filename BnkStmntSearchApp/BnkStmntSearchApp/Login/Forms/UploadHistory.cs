﻿using System;
using System.Windows.Forms;
using BnkStmntSearch.Data.Entities.Manager;

namespace BnkStmntSearchApp.Login.Forms
{
    public partial class UploadHistory : Form
    {
        public UploadHistory()
        {
            InitializeComponent();
            FillUploadDataGrid();
        }

        private void FillUploadDataGrid()
        {
            var uploadHistoryMgr = new UploadHistoryManager();
            foreach (var history in uploadHistoryMgr.GetList())
                uploadDataGrid.Rows.Add(history.FileName, history.UploadedBy,
                    history.GetUploadDate().ToString("MM/dd/yyyy"));
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}