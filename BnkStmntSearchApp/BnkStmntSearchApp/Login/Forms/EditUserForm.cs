﻿using System;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using BnkStmntSearch.Data.Entities;
using BnkStmntSearch.Data.Entities.Manager;
using BnkStmntSearch.Data.Enum;
/*using BnkStmntSearch.NHibernate.Enum;
using BnkStmntSearch.NHibernate.Repo;*/
using log4net;

namespace BnkStmntSearchApp.Login.Forms
{
    public partial class EditUserForm : Form
    {
        public static LoggedUser CurrentUser = LoggedUser.Instance;

        private ILog _logger;
        private readonly UserManager _userManager;
        private readonly User _userToUpdate;

        public EditUserForm()
        {
            InitializeComponent();
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _userManager = new UserManager();
            PwdTextbox.Enabled = false;
            ConfPwdTextbox.Enabled = false;
        }

        public EditUserForm(User user)
            : this()
        {
            _userToUpdate = user;
            LoadUserToEditForm();
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(out var message))
            {
                MessageBox.Show(this, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            _userToUpdate.UserId = UserIdTextbox.Text;
            if (ChangePassword())
                _userToUpdate.Password = UserManager.AesEncryptString(PwdTextbox.Text);
            _userToUpdate.Role = UserManager.GetUserRole(RoleComboBox.Text);

            if (_userManager.UpdateUser(_userToUpdate))
            {
                MessageBox.Show(this, $"{UserIdTextbox.Text} is successfuly saved", "Edit User", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(this, "An error encountered on saving user to database.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private bool ValidateForm(out string message)
        {
            var messageStringBuilder = new StringBuilder();
            var isValid = true;
            message = null;

            if (string.IsNullOrEmpty(OldPwdTextbox.Text) &&
                string.IsNullOrEmpty(PwdTextbox.Text) &&
                string.IsNullOrEmpty(ConfPwdTextbox.Text))
                return true;
            if (OldPwdTextbox.Text == UserManager.AesDecryptString(_userToUpdate.Password))
                if (PwdTextbox.Text == ConfPwdTextbox.Text)
                {
                    return true;
                }
                else
                {
                    message = "New password does not match Confirmation password.";
                    return false;
                }
            if (OldPwdTextbox.Text != UserManager.AesDecryptString(_userToUpdate.Password))
            {
                message = "The entered old password does not match user's current password.";
                return false;
            }

            if (string.IsNullOrEmpty(UserIdTextbox.Text))
            {
                messageStringBuilder.Append("User ID must not be blank");
                isValid = false;
            }
            if (UserManager.GetUserRole(RoleComboBox.Text) == UserRole.NotSet)
            {
                messageStringBuilder.AppendLine("Entered role is not valid");
                isValid = false;
            }

            message = isValid ? null : messageStringBuilder.ToString();
            return isValid;
        }

        private bool ChangePassword()
        {
            return !string.IsNullOrEmpty(OldPwdTextbox.Text) &&
                   !string.IsNullOrEmpty(PwdTextbox.Text) &&
                   !string.IsNullOrEmpty(ConfPwdTextbox.Text);
        }


        private void LoadUserToEditForm()
        {
            UserIdTextbox.Text = _userToUpdate.UserId;
            RoleComboBox.Text = _userToUpdate.Role.ToString();
        }

        private void OldPwdTextbox_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(OldPwdTextbox.Text)) return;
            PwdTextbox.Enabled = true;
            ConfPwdTextbox.Enabled = true;
        }
    }
}