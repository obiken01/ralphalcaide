﻿using System;
using System.Reflection;
using System.Windows.Forms;
using BnkStmntSearch.Data.Entities.Manager;
using log4net;
//using UserManager = BnkStmntSearch.Data.Manager.UserManager;

namespace BnkStmntSearchApp.Login.Forms
{
    public partial class Users : Form
    {
        public static LoggedUser CurrentUser = LoggedUser.Instance;

        private ILog _logger;
        private readonly IUserManager _userManager;

        public Users()
        {
            InitializeComponent();
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            _userManager = new UserManager();
            FillUserDataGrid();
        }

        private void FillUserDataGrid()
        {
            foreach (var user in _userManager.GetList())
                UserDataGrid.Rows.Add(user.Id.ToString(), user.UserId, user.Password, user.Role.ToString());
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void AddUserBtn_Click(object sender, EventArgs e)
        {
            using (var addNewUser = new AddUserForm())
            {
                if (addNewUser.ShowDialog(this) != DialogResult.OK) return;
                UserDataGrid.Rows.Clear();
                FillUserDataGrid();
            }
        }

        private void UserDataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!long.TryParse(UserDataGrid.Rows[e.RowIndex].Cells[0].Value.ToString(), out var value))
                return;
            var selectedUser = _userManager.GetUserById(value);
            using (var editUser = new EditUserForm(selectedUser))
            {
                if (editUser.ShowDialog(this) != DialogResult.OK) return;
                UserDataGrid.Rows.Clear();
                FillUserDataGrid();
            }
        }

        private void DeleteUserBtn_Click(object sender, EventArgs e)
        {
            long selectedIdUser = 0;
            var selectedUserIdText = string.Empty;
            foreach (DataGridViewRow row in UserDataGrid.SelectedRows)
                if (long.TryParse(row.Cells[0].Value.ToString(), out var value))
                {
                    selectedIdUser = value;
                    selectedUserIdText = row.Cells[1].Value.ToString();
                }
            var userToDelete = _userManager.GetUserById(selectedIdUser);
            if (!_userManager.DeleteUser(userToDelete)) return;
            MessageBox.Show(this, $"{selectedUserIdText} was successfully deleted.", "User", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            UserDataGrid.Rows.Clear();
            FillUserDataGrid();
        }

        private void UserDataGrid_SelectionChanged(object sender, EventArgs e)
        {
        }
    }
}