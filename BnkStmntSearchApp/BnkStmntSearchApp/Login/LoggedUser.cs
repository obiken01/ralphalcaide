﻿using System;
using System.Reflection;
using BnkStmntSearch.Data.Entities;
using BnkStmntSearch.Data.Enum;
using log4net;

namespace BnkStmntSearchApp.Login
{
    public class LoggedUser
    {
        private static LoggedUser _instance;
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private LoggedUser()
        {
            IsLoggedIn = false;
            User = null;
            SessionID = Guid.NewGuid().ToString();
        }

        public bool IsLoggedIn { get; private set; }
        public User User { get; private set; }
        public string SessionID { get; private set; }

        public static LoggedUser Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new LoggedUser();
                return _instance;
            }
        }

        public void Login(User user)
        {
            User = user ?? throw new ArgumentNullException("user");
            IsLoggedIn = true;
            _logger.Info($"Session ID: {SessionID}|User: {User.UserId} logged in");
        }

        public void Logout()
        {
            if (!IsLoggedIn) return;

            _logger.Info($"Session ID: {SessionID}|User: {User.UserId} logged out");
            IsLoggedIn = false;
            User = null;
            SessionID = Guid.NewGuid().ToString();
        }

        public bool IsUserPowerAdmin()
        {
            return User != null && User.Role == UserRole.PowerAdmin;
        }
    }
}