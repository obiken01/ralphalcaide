﻿using log4net;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace BnkStmntSearchApp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            var logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            try
            {
                var product = ((AssemblyProductAttribute)Attribute.GetCustomAttribute(
                    Assembly.GetExecutingAssembly(), typeof(AssemblyProductAttribute), false))
                   .Product;

                logger.Info($"Starting {product}");
            }
            catch(Exception e)
            {
                logger.Error(e);
                throw;
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}
