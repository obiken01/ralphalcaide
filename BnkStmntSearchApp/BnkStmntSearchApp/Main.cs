﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using BnkStmntSearch.Data.Manager;
using BnkStmntSearchApp.Login;
using BnkStmntSearchApp.Login.Forms;
using StAta.Properties;
using log4net;
using BnkStmntSearch.Data.Enum;
using StAta;
using LoginForm = BnkStmntSearchApp.Login.Forms.Login;

namespace BnkStmntSearchApp
{
    public partial class Main : Form
    {
        public static LoggedUser CurrentUser = LoggedUser.Instance;
        private readonly IList<Search> _searchPanels = new List<Search>();
        private FileArchiveManager _fileArchiveManager;
        private ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Main()
        {
            InitializeComponent();
            userIdLabel.Alignment = ToolStripItemAlignment.Right;
            _fileArchiveManager = new FileArchiveManager(CurrentUser.User, Settings.Default.arcvhiveFileDropLocation);
            _searchPanels.Add(new Search());
            DisplaySearchItems();
        }

        private void RestrictUserDisplay()
        {
            switch (CurrentUser.User.Role)
            {
                case UserRole.Staff:
                    toolStripButton3.Visible = false;
                    userToolStripMenuItem.Visible = false;
                    break;
                case UserRole.Officer:
                    toolStripButton3.Visible = true;
                    userToolStripMenuItem.Visible = false;
                    break;
                case UserRole.PowerAdmin:
                    toolStripButton3.Visible = true;
                    userToolStripMenuItem.Visible = true;
                    break;
            }
        }

        private void DisplayLoginScreen()
        {
            if (!CurrentUser.IsLoggedIn)
                using (var login = new LoginForm())
                {
                    if (login.ShowDialog(this) != DialogResult.OK)
                    {
                        Application.Exit();
                        return;
                    }
                }
            RestrictUserDisplay();
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            userIdLabel.Text = $"User ID: {CurrentUser.User.UserId}";
            versionLabel.Text = $"Version: {version}";
            _fileArchiveManager = new FileArchiveManager(CurrentUser.User, Settings.Default.arcvhiveFileDropLocation);
            if(!FileArchiveBackgroundWorker.IsBusy)
                FileArchiveBackgroundWorker.RunWorkerAsync();
        }

        private void DisplaySearchItems()
        {
            foreach (var search in _searchPanels)
                searchPanel.Controls.Add(search);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            foreach (Search searchPanelControl in searchPanel.Controls)
                searchPanelControl.SearchApplicationNumber();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            var fileUploadManager = new FileProcessManager(CurrentUser.User);
            using (var fileDialog = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "text|*.txt"
            })
            {
                if (fileDialog.ShowDialog() != DialogResult.OK) return;
                var isSuccessful = true;
                foreach (var fileName in fileDialog.FileNames)
                    if (!fileUploadManager.FileUpload(fileName))
                        isSuccessful = false;
                if (isSuccessful)
                    MessageBox.Show(this, "File upload Successful", "File Upload", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                else

                    MessageBox.Show(this, "File upload Failed", "File Upload", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            var searchP = new Search();
            _searchPanels.Add(searchP);
            searchPanel.Controls.Add(searchP);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            var firstPanel = searchPanel.Controls[0];
            searchPanel.Controls.Clear();
            searchPanel.Controls.Add(firstPanel);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            DisplayLoginScreen();
        }

        private void uploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentUser.Logout();
            DisplayLoginScreen();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CurrentUser.Logout();
            Application.Exit();
        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var userForm = new Users())
            {
                userForm.ShowDialog(this);
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            CurrentUser.Logout();
        }

        private void FileArchiveBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!int.TryParse(Settings.Default.FileArchiveInterval, out var interval))
                interval = 0;
            while (true) //bug: please code review this
            {
                Thread.Sleep(interval);
                _fileArchiveManager?.ArchiveFiles();
            }
        }

        private void uploadHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var uploadHistory = new UploadHistory())
            {
                uploadHistory.ShowDialog(this);
            }
        }
    }
}