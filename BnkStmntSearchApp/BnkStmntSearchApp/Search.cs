﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BnkStmntSearch.Data.Entity;
using BnkStmntSearch.Data.Manager;

namespace StAta
{
    public partial class Search : UserControl
    {
        public Search()
        {
            InitializeComponent();
            SearchResultDetailsDataGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }

        public Search(int width)
            :this()
        {
            Width = width;
        }

        public void SearchApplicationNumber()
        {
            if(ApplicationNumberTextBox.Text == string.Empty) return;
            var result = SearchBankStatement(ExtractApplicationNumber(ApplicationNumberTextBox.Text));
            FillSearchResultGrid(result);
            //DisplayResult(0);
        }

        private static IList<SearchResult> SearchBankStatement(IList<string> applicationNumbers)
        {
            try
            {
                var mgr = new SearchBankStatementManager();
                return mgr.SearchResultByApplicationNumber(applicationNumbers);
            }
            catch (Exception)
            {
                MessageBox.Show(null, "ERROR", "An error was encountered, please see the application's log file.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var searchResults = SearchBankStatement(ExtractApplicationNumber(ApplicationNumberTextBox.Text)); // todo
            FillSearchResultGrid(searchResults);
            //DisplayResult(0);
        }

        private IList<string> ExtractApplicationNumber(string source)
        {
            var appItems = source.Split(new string[] {"\r\n", "\r", "\n"}, StringSplitOptions.None);
            var result = new List<string>();
            var willFilterAppNum = source.Contains("|-------") || (source.Contains("Application No") && source.Contains("Foreign"));
            var splitChar = source.Contains("|-------") ? '|' : '\t';
            foreach (var lineAppItem in appItems)
            {
                if(string.IsNullOrEmpty(lineAppItem)) continue;

                if (!willFilterAppNum)
                    result.Add(lineAppItem);
                else
                {
                    var itemsForApp = lineAppItem.Split(splitChar);
                    string appItem;
                    if (splitChar == '|')
                        appItem = itemsForApp.Length > 6 ? itemsForApp[5].Trim() : string.Empty;
                    else
                        appItem = itemsForApp.Length > 3 ? itemsForApp[2].Trim() : string.Empty;
                    if(!string.IsNullOrEmpty(appItem))
                        result.Add(appItem);
                }
            }
            return result;
        }

        private void FillSearchResultGrid(IList<SearchResult> searchResults)
        {
            if (searchResults.Count == 0) return;
            //OldSearchResultDataGrid.Rows.Clear();
            SearchResultDetailsDataGrid.Rows.Clear();
            foreach (var searchResult in searchResults)
            {
                /*OldSearchResultDataGrid.Rows.Add(searchResult.Id.ToString(),
                    searchResult.ApplicationNumber,
                    searchResult.FileName,
                    searchResult.UploadDate.ToString("MM/dd/yyyy"),
                    searchResult.Content);*/

                SearchResultDetailsDataGrid.Rows.Add(
                    searchResult.Id,
                    searchResult.ApplicationNumber,
                    searchResult.Content,
                    searchResult.Bank);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ApplicationNumberTextBox.Text = string.Empty;
            SearchResultDetailsDataGrid.Rows.Clear();
        }

        /*private void DisplayResult(int rowIndex)
        {
            if (OldSearchResultDataGrid.Rows[rowIndex].Cells[3].Value == null) return;
            var content = OldSearchResultDataGrid.Rows[rowIndex].Cells[4].Value.ToString();

            SearchResultTextBox.Clear();
            SearchResultTextBox.Text = Regex.Replace(content, @"[^a-zA-Z0-9/*\\:_\-. \r\n]", string.Empty);

            var appNum = OldSearchResultDataGrid.Rows[rowIndex].Cells[1].Value.ToString();
            var strIndex = SearchResultTextBox.Text.IndexOf(appNum);
            SearchResultTextBox.Select(strIndex, appNum.Length);
            SearchResultTextBox.SelectionColor = Color.Red;
        }*/
    }
}