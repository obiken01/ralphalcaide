﻿using BnkStmntSearch.NHibernate.Repo;
using FluentNHibernate.Mapping;

namespace BnkStmntSearch.NHibernate.Map
{
    public class StatementLineRepoMap : ClassMap<StatementLine>
    {
        public StatementLineRepoMap()
        {
            Table("[StatementLine]");
            Id(i => i.Id);
            Map(i => i.Sequence).Column("Sequence");
            Map(i => i.Date).Column("Date");
            Map(i => i.Currency).Column("Currency");
            Map(i => i.Amount).Column("Amount");
            Map(i => i.ApplicationNumber).Not.Nullable().Column("ApplicationNumber");
            Map(i => i.RefServer).Column("RefServer");
            Map(i => i.ChequeNo).Column("ChequeNo");

            References(i => i.BankStatement)
                .Class<BankStatement>()
                .Column("BankStatementId")
                .Cascade.All();
        }
    }
}