﻿using BnkStmntSearch.NHibernate.Repo;
using FluentNHibernate.Mapping;

namespace BnkStmntSearch.NHibernate.Map
{
    public class BankStatementRepoMap : ClassMap<BankStatement>
    {
        public BankStatementRepoMap()
        {
            Table("[BankStatement]");
            Id(i => i.Id);
            Map(i => i.LogicalTerminalDateTime).Column("LogicalTerminalDateTime");
            Map(i => i.HPage).Column("HPage");
            Map(i => i.GBasicHeader).Column("GBasicHeader");
            Map(i => i.GApplication).Column("GApplication");
            Map(i => i.Trn).Column("Trn");
            Map(i => i.AccountIdentification).Column("AccountIdentification");
            Map(i => i.StatementNo).Column("StatementNo");
            Map(i => i.StatementSeqNo).Column("StatementSeqNo");
            Map(i => i.OpenBalanceIndicator).Column("OpenBalanceIndicator");
            Map(i => i.OpenBalanceDate).Column("OpenBalanceDate");
            Map(i => i.OpenBalanceCurrency).Column("OpenBalanceCurrency");
            Map(i => i.OpenBalanceAmount).Column("OpenBalanceAmount");
            Map(i => i.StatementContent).Length(4001).Column("StatementContent");

            HasMany(i => i.StatementLines)
                .KeyColumn("BankStatementId")
                .Inverse()
                .Cascade.AllDeleteOrphan();
            
            References(i => i.FileInfo)
                .Class<FileInfo>()
                .Column("FileInfoId")
                .Cascade.All();
        }
    }
}