﻿using BnkStmntSearch.NHibernate.Enum;
using BnkStmntSearch.NHibernate.Repo;
using FluentNHibernate.Mapping;

namespace BnkStmntSearch.NHibernate.Map
{
    public class UserRepoMap : ClassMap<User>
    {
        public UserRepoMap()
        {
            Table("[User]");
            Id(i => i.Id);
            Map(i => i.UserId).Column("UserId");
            Map(i => i.Password).Column("Password");
            Map(i => i.Role).CustomType<UserRole>().Column("Role");
        }
    }
}