﻿using BnkStmntSearch.NHibernate.Repo;
using FluentNHibernate.Mapping;

namespace BnkStmntSearch.NHibernate.Map
{
    public class FileInfoRepoMap : ClassMap<FileInfo>
    {
        public FileInfoRepoMap()
        {
            Table("[FileInfo]");
            Id(i => i.Id);
            Map(i => i.FileName).Column("FileName");
            Map(i => i.UploadDate).Column("UploadDate");
            Map(i => i.File)
                .Column("[File]")
                .CustomSqlType("varbinary(max)")
                .Length(int.MaxValue);
            Map(i => i.UploadedBy).Column("UploadedBy");
            
            HasMany(i => i.BankStatements)
                .KeyColumn("FileInfoId")
                .Inverse()
                .Cascade.AllDeleteOrphan();
        }
    }
}