﻿using BnkStmntSearch.NHibernate.Repo;
using FluentNHibernate.Mapping;

namespace BnkStmntSearch.NHibernate.Map
{
    public class UploadHistoryRepoMap : ClassMap<UploadHistory>
    {
        public UploadHistoryRepoMap()
        {
            Table("[UploadHistory]");
            Id(i => i.Id);
            Map(i => i.FileName).Column("FileName");
            Map(i => i.UploadDate).Column("UploadDate");
            Map(i => i.UploadedBy).Column("UploadedBy");
        }
    }
}