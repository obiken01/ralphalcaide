﻿using System;

namespace BnkStmntSearch.NHibernate.Repo
{
    public class UploadHistory
    {
        public virtual long Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual DateTime UploadDate { get; set; }
        public virtual string UploadedBy { get; set; }
    }
}