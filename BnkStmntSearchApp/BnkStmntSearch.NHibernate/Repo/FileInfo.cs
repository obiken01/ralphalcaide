﻿using System;
using System.Collections.Generic;

namespace BnkStmntSearch.NHibernate.Repo
{
    public class FileInfo
    {
        public virtual long Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual DateTime UploadDate { get; set; }
        public virtual byte[] File { get; set; }
        public virtual string UploadedBy { get; set; }
        public virtual IList<BankStatement> BankStatements { get; set; }
    }
}