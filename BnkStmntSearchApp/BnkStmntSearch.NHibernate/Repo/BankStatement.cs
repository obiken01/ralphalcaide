﻿using System.Collections.Generic;

namespace BnkStmntSearch.NHibernate.Repo
{
    public class BankStatement
    {
        public BankStatement()
        {
            StatementLines = new List<StatementLine>();
        }

        public virtual long Id { get; set; }
        public virtual string LogicalTerminalDateTime { get; set; }
        public virtual string HPage { get; set; }
        public virtual string GBasicHeader { get; set; }
        public virtual string GApplication { get; set; }
        public virtual string Trn { get; set; }
        public virtual string AccountIdentification { get; set; }
        public virtual string StatementNo { get; set; }
        public virtual string StatementSeqNo { get; set; }
        public virtual string OpenBalanceIndicator { get; set; }
        public virtual string OpenBalanceDate { get; set; }
        public virtual string OpenBalanceCurrency { get; set; }
        public virtual string OpenBalanceAmount { get; set; }
        public virtual IList<StatementLine> StatementLines { get; set; }
        public virtual string StatementContent { get; set; }
        public virtual long FileInfoId { get; set; }
        public virtual FileInfo FileInfo { get; set; }
    }
}