﻿namespace BnkStmntSearch.NHibernate.Repo
{
    public class StatementLine
    {
        public virtual long Id { get; set; }
        public virtual string Sequence { get; set; }
        public virtual string Date { get; set; }
        public virtual string Currency { get; set; }
        public virtual string Amount { get; set; }
        public virtual string ApplicationNumber { get; set; } //todo: ref-owner
        public virtual string RefServer { get; set; }
        public virtual string ChequeNo { get; set; }
        public virtual long BankStatementId { get; set; }
        public virtual BankStatement BankStatement { get; set; }
    }
}