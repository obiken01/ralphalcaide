﻿using BnkStmntSearch.NHibernate.Enum;

namespace BnkStmntSearch.NHibernate.Repo
{
    public class User
    {
        public virtual long Id { get; set; }
        public virtual string UserId { get; set; }
        public virtual string Password { get; set; }
        public virtual UserRole Role { get; set; }
    }
}