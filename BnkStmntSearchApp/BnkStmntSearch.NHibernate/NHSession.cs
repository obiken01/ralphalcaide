﻿using System.Configuration;
using BnkStmntSearch.NHibernate.Map;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace BnkStmntSearch.NHibernate
{
    public static class NHSession
    {
        public static ISession OpenSession()
        {
            var connStr = ConfigurationManager.ConnectionStrings["StAtaDbConn"].ToString();
            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                    .ConnectionString(connStr)
                )
                .Mappings(m =>
                    m.FluentMappings
                        .AddFromAssemblyOf<UserRepoMap>()
                        .AddFromAssemblyOf<FileInfoRepoMap>()
                        .AddFromAssemblyOf<StatementLineRepoMap>()
                        .AddFromAssemblyOf<BankStatementRepoMap>()
                        .AddFromAssemblyOf<UploadHistoryRepoMap>())
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                .BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}