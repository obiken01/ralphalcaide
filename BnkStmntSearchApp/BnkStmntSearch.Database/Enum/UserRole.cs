﻿using System.ComponentModel;

namespace BnkStmntSearch.Database.Enum
{
    public enum UserRole
    {
        NotSet = 0,
        [Description("Staff")] Staff = 1,
        [Description("Officer")] Officer,
        [Description("Power Admin")] PowerAdmin
    }
}