﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace BnkStmntSearch.Database.Model.Light
{
    [Table(Name = "StatementLine")]
    public class StatementLine
    {
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public long Id { get; set; }

        [Column(Name = "Sequence", DbType = "VARCHAR")]
        public string Sequence { get; set; }

        [Column(Name = "Date", DbType = "VARCHAR")]
        public string Date { get; set; }

        [Column(Name = "Currency", DbType = "VARCHAR")]
        public string Currency { get; set; }

        [Column(Name = "Amount", DbType = "VARCHAR")]
        public string Amount { get; set; }

        [Column(Name = "ApplicationNumber", DbType = "VARCHAR")]
        public string ApplicationNumber { get; set; }

        [Column(Name = "RefServer", DbType = "VARCHAR")]
        public string RefServer { get; set; }

        [Column(Name = "ChequeNo", DbType = "VARCHAR")]
        public string ChequeNo { get; set; }

        [Column(Name = "Guid", DbType = "VARCHAR")]
        public string Guid { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("BankStatement")]
        [Column(Name = "BankStatementId", DbType = "INTEGER")]
        public long BankStatementId { get; set; }
        public BankStatement BankStatement { get; set; }
    }
}
