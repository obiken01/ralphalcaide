﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace BnkStmntSearch.Database.Model.Light
{
    [Table(Name = "FileInfo")]
    public class FileInfo
    {
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public long Id { get; set; }

        [Column(Name = "FileName", DbType = "VARCHAR")]
        public string FileName { get; set; }

        [Column(Name = "UploadDate", DbType = "VARCHAR")]
        public string UploadDate { get; set; }

        [Column(Name = "UploadedBy", DbType = "VARCHAR")]
        public string UploadedBy { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("FileInfoId")]
        public IList<BankStatement> BankStatements { get; set; }
    }
}