﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace BnkStmntSearch.Database.Model
{
    [Table(Name = "BankStatement")]
    public class BankStatement
    {
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public long Id { get; set; }

        [Column(Name = "LogicalTerminalDateTime", DbType = "VARCHAR")]
        public string LogicalTerminalDateTime { get; set; }

        [Column(Name = "HPage", DbType = "VARCHAR")]
        public string HPage { get; set; }

        [Column(Name = "GBasicHeader", DbType = "VARCHAR")]
        public string GBasicHeader { get; set; }

        [Column(Name = "GApplication", DbType = "VARCHAR")]
        public string GApplication { get; set; }

        [Column(Name = "Trn", DbType = "VARCHAR")]
        public string Trn { get; set; }

        [Column(Name = "AccountIdentification", DbType = "VARCHAR")]
        public string AccountIdentification { get; set; }

        [Column(Name = "StatementNo", DbType = "VARCHAR")]
        public string StatementNo { get; set; }

        [Column(Name = "StatementSeqNo", DbType = "VARCHAR")]
        public string StatementSeqNo { get; set; }

        [Column(Name = "OpenBalanceIndicator", DbType = "VARCHAR")]
        public string OpenBalanceIndicator { get; set; }

        [Column(Name = "OpenBalanceDate", DbType = "VARCHAR")]
        public string OpenBalanceDate { get; set; }

        [Column(Name = "OpenBalanceCurrency", DbType = "VARCHAR")]
        public string OpenBalanceCurrency { get; set; }

        [Column(Name = "OpenBalanceAmount", DbType = "VARCHAR")]
        public string OpenBalanceAmount { get; set; }

        [Column(Name = "StatementContent", DbType = "VARCHAR")]
        public string StatementContent { get; set; }

        [Column(Name = "Guid", DbType = "VARCHAR")]
        public string Guid { get; set; }
        
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("FileInfo")]
        [Column(Name = "FileInfoId", DbType = "INTEGER")]
        public long FileInfoId { get; set; }
        public FileInfo FileInfo { get; set; }
        
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("BankStatementId")]
        public IList<StatementLine> StatementLines { get; set; }
    }
}