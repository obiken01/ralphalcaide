﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using BnkStmntSearch.Database.Enum;

namespace BnkStmntSearch.Database.Model
{
    [Table(Name = "User")]
    public class User
    {
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public long Id { get; set; }

        [Column(Name = "UserId", DbType = "VARCHAR")]
        public string UserId { get; set; }

        [Column(Name = "Password", DbType = "VARCHAR")]
        public string Password { get; set; }

        [Column(Name = "Role", DbType = "INTEGER")]
        public UserRole Role { get; set; }
    }
}