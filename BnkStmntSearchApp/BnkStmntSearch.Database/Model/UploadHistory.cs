﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace BnkStmntSearch.Database.Model
{
    [Table(Name = "UploadHistory")]
    public class UploadHistory
    {
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public long Id { get; set; }

        [Column(Name = "FileName", DbType = "VARCHAR")]
        public string FileName { get; set; }

        [Column(Name = "UploadDate", DbType = "VARCHAR")]
        public string UploadDate { get; set; }

        [Column(Name = "UploadedBy", DbType = "VARCHAR")]
        public string UploadedBy { get; set; }
    }
}