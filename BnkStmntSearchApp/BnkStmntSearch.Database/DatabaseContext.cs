﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using BnkStmntSearch.Database.Model;
using BnkStmntSearch.Database.Model.Light;

namespace BnkStmntSearch.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() :
            base(new SQLiteConnection
            {
                ConnectionString = new SQLiteConnectionStringBuilder
                {
                    DataSource = @"Database/BnkStmntSearchDb.db",
                    ForeignKeys = true
                }.ConnectionString
            }, true)
        {
        }

        //Insert DB models here

        public DbSet<User> User { get; set; }
        public DbSet<UploadHistory> UploadHistory { get; set; }
        public DbSet<BnkStmntSearch.Database.Model.FileInfo> FileInfo { get; set; }
        public DbSet<BnkStmntSearch.Database.Model.BankStatement> BankStatement { get; set; }
        public DbSet<BnkStmntSearch.Database.Model.StatementLine> StatementLine { get; set; }
/*

        public DbSet<BnkStmntSearch.Database.Model.Light.FileInfo> LightFileInfo { get; set; }
        public DbSet<BnkStmntSearch.Database.Model.Light.BankStatement> LightBankStatement { get; set; }
        public DbSet<BnkStmntSearch.Database.Model.Light.StatementLine> LightStatementLine { get; set; }
*/


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
